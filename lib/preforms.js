let component_data_print = function () {
  return {
    "type": "expression",
    "children": [{
      "type": "sub_expression",
      "children": [{
        "type": "keyword",
        "content": "arrayify("
      }, {
        "type": "variable",
        "content": "_data"
      }, {
        "type": "keyword",
        "content": ",true)"
      }, {
        "type": "op",
        "op": "||"
      }, {
        "type": "array_definition",
        "children": []
      }]
    }, {
      "type": "sub_variable",
      "content": ".map"
    }, {
      "type": "call_parameters",
      "children": [{
        "type": "function_definition",
        "children": [{
          "type": "function_parameters",
          "children": [{
            "type": "parameter_name",
            "content": "data"
          }]
        }, {
          "type": "function_body",
          "children": [{
            "type": "keyword",
            "content": "return "
          }, {
            "type": "string",
            "content": "data-",
            children: []
          }, {
            "type": "op",
            "op": "+"
          }, {
            "type": "variable",
            "content": "data"
          }, {
            "type": "array_access",
            "children": [{
              "type": "number",
              "content": "0"
            }]
          }, {
            "type": "sub_variable",
            "content": ".replace(/_/g,\"-\")",
            children: []
          }, {
            "type": "op",
            "op": "+"
          }, {
            "type": "string",
            "content": "=\\\""
          }, {
            "type": "op",
            "op": "+"
          }, {
            "type": "keyword",
            "content": "_clean(data[1])"
          }, {
            "type": "op",
            "op": "+"
          }, {
            "type": "string",
            "content": "\\\""
          }, {
            "type": "op",
            "op": ";"
          }]
        }]
      }]
    }, {
      "type": "sub_variable",
      "content": ".join"
    }, {
      "type": "call_parameters",
      "children": [{
        "type": "string",
        "content": " "
      }]
    }]
  };
};

let component_css_link = function (names) {
  return {
    "type": "tag",
    "children": [{
        "type": "tag_parameter",
        "name": "rel",
        "children": [{
          "type": "tag_attribute_content",
          "content": "stylesheet"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "type",
        "children": [{
          "type": "tag_attribute_content",
          "content": "text/css"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "href",
        "children": [{
          "type": "tag_attribute_content",
          "content": "/components/css/" + names.join('%7C')
        }]
      },
      {
        type: 'tag_close',
        children: []
      }
    ],
    "name": "component_link",
    "self_close": true
  }
}

let component_js_script = function (names) {
  return {
    "type": "tag",
    "children": [{
        "type": "tag_parameter",
        "name": "type",
        "children": [{
          "type": "tag_attribute_content",
          "content": "text/javascript"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "src",
        "children": [{
          "type": "tag_attribute_content",
          "content": "/components/js/" + names.join('%7C')
        }]
      },
      {
        type: 'tag_close',
        children: []
      }
    ],
    "name": "component_script"
  }
}

let component_component_script = function (names) {
  return {
    "type": "tag",
    "children": [{
        "type": "tag_parameter",
        "name": "type",
        "children": [{
          "type": "tag_attribute_content",
          "content": "text/javascript"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "src",
        "children": [{
          "type": "tag_attribute_content",
          "content": "/components/components/" + names.join('%7C')
        }]
      },
      {
        type: 'tag_close',
        children: []
      }
    ],
    "name": "component_constructor_script"
  }
}

let required_stylesheet = function (link) {
  return {
    "type": "tag",
    "children": [{
        "type": "tag_parameter",
        "name": "rel",
        "children": [{
          "type": "tag_attribute_content",
          "content": "stylesheet"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "type",
        "children": [{
          "type": "tag_attribute_content",
          "content": "text/css"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "href",
        "children": [{
          "type": "tag_attribute_content",
          "content": link
        }]
      },
      {
        type: 'tag_close',
        children: []
      }
    ],
    "name": "required_stylesheet",
    "self_close": true
  }
}

let required_script = function (link) {
  return {
    "type": "tag",
    "children": [{
        "type": "tag_parameter",
        "name": "type",
        "children": [{
          "type": "tag_attribute_content",
          "content": "text/javascript"
        }]
      },
      {
        "type": "tag_parameter",
        "name": "src",
        "children": [{
          "type": "tag_attribute_content",
          "content": link
        }]
      },
      {
        type: 'tag_close',
        children: []
      }
    ],
    "name": "required_script"
  }
}

module.exports = {
  component_data_print: component_data_print,
  component_css_link: component_css_link,
  component_js_script: component_js_script,
  component_component_script: component_component_script,
  required_script: required_script,
  required_stylesheet: required_stylesheet
}