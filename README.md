# Introduction To Aleet

**Aleet** is a server-side templating engine that supports a custom web-component system designed to support encapsulated code with the bare minumum of client-side overhead.

# Installing

    npm install aleet -save

# Setup
**Aleet** was origionally built with **Express JS** in mind. The setup for express is short and simple.

    const aleet = require('aleet');

    const options = {
	   debug:true, //set to false before deployment
	   viewPath: path.join(__dirname, 'views'), //path to views folder
	  }

    aleet.config(options);
    express.engine('html,aleet); //recommend using html files for built in syntax highlighting

    //optional routes (required for component support)
    express.get('/components/components/:name',aleet.components);
    express.get('/components/css/:name',aleet.css);
    express.get('/components/js/:name',aleet.js);

# Templating

The templating engine supports template inheritance and inclusion.<br><br>

**Subs** get replaced by the immidiate child template<br>

*layout.html*

    ...
    <div  id="main-content">
      <sub  name="main">
        <div class="gets_replaced"></div>
      </sub>
    </div>
    ...

*child.html*

    <inherits name="/layout"></inherits>

    <sub  name="main">
      <div  class="col">
        test
      </div>
    </sub>
    
  *output*
  
    ...
    <div  id="main-content">
      <div  class="col">
        test
      </div>
    </div>
    ...
<br><br>

**Stacks** get appended to by all child templates.

*layout.html*

    ...
    <stack name="head_css">
      <link href="/app.css">
    </stack>
    ...

*child.html*

    <inherits name="/layout"></inherits>

    <stack name="head_css">
      <link href="/child.css">
    </stack>
    
  *output*
  
    ...
    <link href="/app.css">
    <link href="/child.css">
    ...
<br><br><br>

Data passed to the render function can be interpolated into the resulting html<br><br>
*test.html*

    ...
    <ul>
      <li>
        sanatized data insertion <% unsafe %>
      </li>
      <li>
        unsanitized data insertion <%! unsafe %>
      </li>
      @if(1 > 2)
        <li>
          This will show not up
        </li>
      @elseif(1 < 2)
        <li>
          this will show up
        </li>
      @endif
      @each( item in arr)
        <li>
          <% item %>
        </li>
      @endeach
      @each( prop in obj)
        <li>
          <% obj[prop] %>
        </li>
      @endeach
    </ul>
    ...

*data*

    {
	    unsafe:"<bold> Unsafe </bold>",
	    arr:[1,2,3],
	    obj:{a:'a',b:'b',c:'c'}
    }

*output*

    ...
    <ul>
      <li>
        sanatized data insertion &lt;bold&gt; Unsafe &lt;/bold&gt;
      </li>
      <li>
        unsanitized data insertion <bold> Unsafe </bold>
      </li>
      <li>
        this will show up
      </li>
      <li>
        1
      </li>
      <li>
        2
      </li>
      <li>
        3
      </li>
      <li>
        a
      </li>
      <li>
        b
      </li>
      <li>
        c
      </li>
    </ul>
    ...


## Components

**Aleet** has some unique features that enabled and empower components within your design. Lets start with an example.

*example.html*

    <require name="test-comp" path="path/from/view/folder/test-comp" version="latest">
    <html>
      <head>
        <title>TITLE</title>
        <link rel="stylesheet" type="text/css" href="/components/css/_base">
        <stack  name="components_css">
        </stack>
      </head>
      <body>
        <test-comp name="<% customer.name %>" age="<% customer.age %>"></comp>
        <script type="text/javascript" src="/components/js/_base"></script>
        <stack  name="components_js"></stack>
      </body>
    </html>

test-comp.html

    <style>
      .test-comp{
        background-color:green;
      }
    </style>

    <template>
      <div  class="test-comp aleet-comp"  name="<%name%>"  age="<%!age%>">
        My Name is 
        <span class="nameSpan"><% name %></span> 
        and I am 
        <span class="ageSpan"><% age %></span>
      </div>
    </template>

    <script>
      aleet.register_component('test-comp',{
        ready:function(){
          this[0].innerHTML+=" Edited By Javascript!";
        },
        attributes:{
          name:'String',
          age:'Number'
        },
        on_name_change:function(){
          this.$$('.nameSpan').elem().innerHTML = this.attr('name');
        },
        on_age_change:function(){
          this.$$('.ageSpan').elem().innerHTML = this.attr('age');
        }
      });
    </script>

*data*

    {
      customer:{
        name:"John Doe",
        age:35
      }
    }

*output*

    <html>
      <head>
        <title>TITLE</title>
        <link rel="stylesheet" type="text/css" href="/components/css/_base">
        <link rel="stylesheet" type="text/css" href="/components/css/test-comp--latest">
      </head>
      <body>
        <div  class="test_comp aleet_comp"  name="John Doe"  age="35">
          My Name is 
          <span class="nameSpan">John Doe</span> 
          and I am 
          <span class="ageSpan">35</span>
        </div>
        <script type="text/javascript" src="/components/js/_base"></script>
        <script type="text/javascript" src="/components/js/test_comp--latest"></script>
      </body>
    </html>

you'll notice that there are two stylesheets and two javascript files imported from our component routes we set up at the beginning. The *_base* stylesheet and script file contain the entirity of our client side runtime, all 12.6 kb (uncompressed) of it. The other two imports point to the style and script inside of our component file.

Every component needs to have the class **aleet-comp** and an additional class that matches the tag name (custom tag names must have a - in order to be considered correct html) that is unique from any other component.
You register the compnent with **aleet.register_component(component_name, component_object)**, the component object can specify a ready function, a supply function, on change handlers, and custom functions

**Aleet** comes with a small client side runtime that supports components and their use.

The basic usage of the client side runtime begins with the **\$\$( selector )** function. This function serves as a shorthand for element selection. **\$\$** always returns an array of matched elements. each element has the following properties.

**\$\$( selector )** This is the same function as before, but bound to this element.<br>
**attr( name, value optional )** This function allows for the reading and updating of attributes on the element.<br>
**data( name, value optional )** This function allows for the reading and updating of arbitrary data on a component, the bahavior with non-component elements is undefined.<br>
**on( event_name, handler )** This function sets a handler to be called when this element experiences an event with specified name.<br>
**trigger( event_name )** This function triggers an event on this
element.<br>
**slot( slot_name )** this function returns the $$ wraped slot inside this component that has the specified name<br>
**classes** alias for the dom element's classList porperty<br>
**styles** alias for the dom element's style property<br>
**elem()** the naked DOM element is returned.

Lets look at an example of how this can be used with the previous example code.

    var comp = $$('.test_comp');
    //returns [{elem},first:func...]
    comp = comp.first();
    comp.attr('name','Jane Doe');
    //sets name to 'Jane Doe' and calls on_name_change

For more information, I suggest looking into our wiki