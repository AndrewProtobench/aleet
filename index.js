const fs = require('fs-extra');
const https = require('https');
const url = require('url');
const path = require('path');
const preforms = require('./lib/preforms.js');


const options = {
  viewPath: "",
  debug: false,
  clientVar: "aleet",
  selectorVar: "$$",
  cssProcessor: async function (input) {
    return input;
  },
  jsProcessor: async function (input) {
    return input;
  },
  stripBlockComments: false,
  log: console.log,
  error: console.error,
};

const viewCache = {};

const cssCache = {};

const jsCache = {};

const componentCache = {};

const selfClosingTags = {
  area: {},
  base: {},
  br: {},
  col: {},
  command: {},
  embed: {},
  hr: {},
  img: {},
  input: {},
  keygen: {},
  link: {},
  meta: {},
  param: {},
  source: {},
  track: {},
  wbr: {},
  inherits: {},
  constructor: {},
  require: {}
};

const keywords = [
  'String',
  'Number',
  'new',
  'for',
  'while',
  'return',
  'var',
  'Object',
  'let',
  'const',
  'parseInt',
  'parseFloat',
  'Date',
  'JSON',
  'true',
  'false',
  'undefined',
  'typeof',
  'instanceof',
]

function pwait(timeout){
  return new Promise(function(res){
    setTimeout(res,timeout);
  });
}

function array_flat(arr) {
  let out = [];
  let l = arr.length;
  for (let i = 0; i < l; i++) {
    out = out.concat(arr[i]);
  }
  return out;
}

function object_copy(obj, array) {
  var clone = {};
  if (array === true) {
    clone = [];
  }
  for (var i in obj) {
    if (obj[i] != null && Array.isArray(obj[i])) {
      clone[i] = object_copy(obj[i], true);
    } else if (obj[i] != null && (typeof (obj[i]) == "object" || obj[i] instanceof Object))
      clone[i] = object_copy(obj[i]);
    else
      clone[i] = obj[i];
  }
  return clone;
}

function HTTPRead(path) {
  return new Promise((resolve, reject) => {
    https.get(path, (resp) => {
      let data = '';

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        resolve(data);
      });

    }).on("error", (err) => {
      reject(err);
    });
  });
}

async function loadComponent(filepath = '', name, version) {
  if (typeof name === 'undefined') {
    name = filepath.match(/([^\\\/]+)([\.][^]+)?$/)[1];
  }
  if (typeof version === 'undefined') {
    version = 'latest';
  }
  let cacheBase = options.cachePath || path.join(options.viewPath, 'cache');
  let cacheFilePath = path.join(cacheBase, name, version);
  if (await fs.exists(cacheFilePath) && (filepath === '' || options.debug === false)) {
    let contents = '';
    while((contents = await fs.readFile(cacheFilePath, 'utf8')).length === 0){
      await pwait(15);
    }
    return contents;
  } else if (filepath) {
    let contents = '';
    let urlPath = url.parse(filepath);
    if (urlPath.protocol) {
      contents = await HTTPRead(urlPath.href);
    } else {
      if (path.extname(filepath) === "") {
        filepath += '.html';
      }
      contents = await fs.readFile(path.join(options.viewPath, filepath), 'utf8')
      if (await fs.exists(path.join(options.viewPath, filepath.replace(/\.[^.]+$/,'.js')))) {
        contents += '\n<script>' + (await fs.readFile(path.join(options.viewPath, filepath.replace(/\.[^.]+$/,'.js')), 'utf8')) +'</script>'
      }
    }
    await fs.ensureDir(path.join(cacheBase, name));
    await fs.writeFile(cacheFilePath, contents, 'utf8');
    return contents;
  } else {
    throw new Error("component not found! " + name ? name : filepath);
  }
}

/**
 * @param {string} contents
 */
function lexer(contents) {
  let tokens = [];
  let modeStack = ['html'];
  let flag = '';
  if (options.stripBlockComments === true) {
    contents = contents.replace(/\/\*([^\*]|[\*][^\/])*\*\//g,'');
  }
  contents = contents.replace(/\\/g,'\uFFFF');
  let l = contents.length;
  for (let i = 0; i < l; i++) {
    let mode = modeStack[modeStack.length - 1];
    let lookBehind = tokens[tokens.length-1];
    let sub = contents.substring(i);
    let window = contents.substring(Math.max(0,i-10),Math.min(l,i+10));
    if (mode === 'html') {
      if (contents[i] === "<" && contents[i + 1] === '!') {
        let content = sub.match(/^<[^>]+>/)[0];
        tokens.push([content, 'doctype']);
        i += content.length - 1;
      } else if (contents[i] === "<" && contents[i + 1] !== '%') {
        tokens.push(['<', 'tag_open']);
        modeStack.push('tag_opened');
      } else if (contents[i] === "<" && contents[i + 1] === '%') {
        if (contents[i + 2] === '!') {
          tokens.push(['<%!', 'expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else if (contents[i + 2] === 'j') {
          tokens.push(['<%j', 'json_expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else {
          tokens.push(['<%', 'clean_expression_open']);
          modeStack.push('expression_opened');
          i++;
        }
      } else if (contents[i] === '@' && /^@if/.test(sub)) {
        tokens.push(['@if', 'if_open']);
        modeStack.push('if_opened');
        i += 2;
      } else if (contents[i] === '@' && /^@elseif/.test(sub)) {
        tokens.push(['@elseif', 'elseif_open']);
        modeStack.push('elseif_opened');
        i += 6;
      } else if (contents[i] === '@' && /^@else/.test(sub)) {
        tokens.push(['@else', 'else_open']);
        i += 4;
      } else if (contents[i] === '@' && /^@each/.test(sub)) {
        tokens.push(['@each', 'each_open']);
        modeStack.push('each_opened');
        i += 4;
      } else if (contents[i] === '@' && /^@endif/.test(sub)) {
        tokens.push(['@endif', 'if_close']);
        i += 5;
      } else if (contents[i] === '@' && /^@endeach/.test(sub)) {
        tokens.push(['@endeach', 'each_close']);
        i += 7;
      } else {
        let content = sub.match(/^(([^<@]|@[^ie])+)/)[1];
        tokens.push([content.replace(/'/g, "&apos;"), 'html_content']);
        i += content.length - 1;
      }
    } else if (mode === 'tag_opened') {
      if (contents[i] === '/') {
        tokens.push(['/', 'tag_closer']);
      } else if (/[a-zA-Z]/.test(contents[i])) {
        let identifier = sub.match(/^([a-zA-Z0-9\-]+)/)[1];
        tokens.push([identifier, 'tag_identifier']);
        modeStack.push('tag_identified');
        i += identifier.length - 1;
        if (tokens.length < 3 || tokens[tokens.length - 2][0] !== '/') {
          if (identifier === 'style') {
            flag = 'style';
          } else if (identifier === 'script') {
            flag = 'script'
          }
        } else {
          flag = '';
        }
      } else if (contents[i] === '!' && contents[i + 1] === '-' && contents[i + 2] === '-') {
        tokens.pop();
        const end = sub.indexOf('-->');
        const content = sub.substring(0, end + 3).replace(/'/g, "&apos;");
        tokens.push(['<'+content, 'html_comment']);
        modeStack.pop();
        i += 2;
      }
    } else if (mode === 'tag_identified') {
      if (/[a-z]/.test(contents[i])) {
        let identifier = sub.match(/^([a-z_:][a-zA-Z\-0-9_:\.]*)/)[1];
        tokens.push([identifier, 'tag_parameter']);
        modeStack.push('tag_parameter');
        i += identifier.length - 1;
      } else if (contents[i] === '>') {
        tokens.push(['>', 'tag_close']);
        while (modeStack[modeStack.length - 1] != 'tag_opened') modeStack.pop();
        modeStack.pop();
        if (flag === 'style') {
          modeStack.push('style');
        } else if (flag === 'script') {
          modeStack.push('script');
        }
      }
    } else if (mode === 'tag_parameter') {
      if (contents[i] === '=') {
        tokens.push(['=', 'tag_attribute_assignment']);
      } else if (contents[i] === '"') {
        tokens.push(['"', 'tag_attribute_open']);
        modeStack.push("tag_attribute_opened");
      } else if (contents[i] === "'") {
        tokens.push(["'", 'tag_single_attribute_open']);
        modeStack.push("tag_single_attribute_opened");
      } else if (contents[i] === ' ') {
        tokens.push([' ', 'tag_attribute_close']);
        modeStack.pop();
      } else if (contents[i] === '>') {
        tokens.push([' ', 'tag_attribute_close']);
        modeStack.pop();
        i--;
      }
    } else if (mode === 'tag_attribute_opened') {
      if (contents[i] === '"') {
        if (tokens[tokens.length - 1][1] === 'tag_attribute_open') {
          tokens.push(['', 'tag_attribute_content']);
        }
        tokens.push(['"', 'tag_attribute_close']);
        while (modeStack[modeStack.length - 1] != 'tag_identified') modeStack.pop();
      } else if (contents[i] === "<" && contents[i + 1] === '%') {
        if (contents[i + 2] === '!') {
          tokens.push(['<%!', 'expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else if (contents[i + 2] === 'j') {
          tokens.push(['<%j', 'json_expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else {
          tokens.push(['<%', 'clean_expression_open']);
          modeStack.push('expression_opened');
          i++;
        }
      } else {
        let content = sub.match(/^(([^<"]|(<[^%]))+)/)[1];
        tokens.push([content, 'tag_attribute_content']);
        i += content.length - 1;
      }
    } else if (mode === 'tag_single_attribute_opened') {
      if (contents[i] === "'") {
        if (tokens[tokens.length - 1][1] === 'tag_single_attribute_open') {
          tokens.push(['', 'tag_single_attribute_content']);
        }
        tokens.push(["'", 'tag_single_attribute_close']);
        while (modeStack[modeStack.length - 1] != 'tag_identified') modeStack.pop();
      } else if (contents[i] === "<" && contents[i + 1] === '%') {
        if (contents[i + 2] === '!') {
          tokens.push(['<%!', 'expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else if (contents[i + 2] === 'j') {
          tokens.push(['<%j', 'json_expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else {
          tokens.push(['<%', 'clean_expression_open']);
          modeStack.push('expression_opened');
          i++;
        }
      } else {
        let content = sub.match(/^(([^<']|(<[^%]))+)/)[1];
        tokens.push([content, 'tag_single_attribute_content']);
        i += content.length - 1;
      }
    } else if (mode === 'style') {
      if (contents[i] === '<' && contents[i + 1] === '/') {
        modeStack.pop();
        i--;
      } else if (contents[i] === '<' && contents[i + 1] === '%') {
        if (contents[i + 2] === '!') {
          tokens.push(['<%!', 'expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else if (contents[i + 2] === 'j') {
          tokens.push(['<%j', 'json_expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else {
          tokens.push(['<%', 'clean_expression_open']);
          modeStack.push('expression_opened');
          i++;
        }
      } else {
        let end = sub.indexOf('</style>');
        let content = sub.substring(0, end);
        tokens.push([content, 'css_content']);
        i += content.length - 1;
      }
    } else if (mode === 'script') {
      if (contents[i] === '<' && contents[i + 1] === '/' && contents[i + 2] === 's') {
        modeStack.pop();
        i--;
      } else if (contents[i] === '<' && contents[i + 1] === '%') {
        if (contents[i + 2] === '!') {
          tokens.push(['<%!', 'expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else if (contents[i + 2] === 'j') {
          tokens.push(['<%j', 'json_expression_open']);
          modeStack.push('expression_opened');
          i += 2;
        } else {
          tokens.push(['<%', 'clean_expression_open']);
          modeStack.push('expression_opened');
          i++;
        }
      } else {
        let end_script = sub.indexOf('</script>');
        let start_sub = sub.indexOf('<%');
        if (start_sub < 0) start_sub = end_script + 1;
        let content = sub.substring(0, Math.min(end_script, start_sub));
        tokens.push([content, 'javascript_content']);
        i += content.length - 1;
      }
    } else if (mode === 'expression_opened') {
      if (contents[i] === '%' && contents[i + 1] === '>') {
        tokens.push(['%>', 'expression_close']);
        modeStack.pop();
        i++;
      } else if (contents[i] === '"') {
        let content = sub.match(/^"(([^"\uFFFF]|\uFFFF"|\uFFFF[^"])*)/)[1];
        i += content.length + 1;
        content = content.replace(/(\uFFFF)?'/g, "'").replace(/(\uFFFF)?"/, '\uFFFF"');
        tokens.push([content, 'string']);
        modeStack.push('variable');
      } else if (contents[i] === "'") {
        let content = sub.match(/^'(([^'\uFFFF]|\uFFFF'|\uFFFF[^'])*)/)[1];
        i += content.length + 1;
        content = content.replace(/(\uFFFF)?'/g, "'").replace(/(\uFFFF)?"/, '\uFFFF"');
        tokens.push([content, 'string']);
        modeStack.push('variable');
      } else if (contents[i] === "/") {
        // if our last token was a number or a pare_expression_close or a sub_pare_expression_close then this is a math operator
        // otherwise it is a regex
        const lookBehind = tokens[tokens.length - 1];
        if (lookBehind[1] === 'number' || lookBehind[1] === 'pare_expression_close' || lookBehind[1] === 'sub_pare_expression_close') {
          tokens.push(["/", 'op']);
        } else {
          let content = sub.match(/^(\/([^\/\uFFFF]|\uFFFF\/|\uFFFF[^\/])*\/([gim]+)?)/);
          if (content && content[1].indexOf('\n') === -1 && (lookBehind[1] === 'op'||lookBehind[1].indexOf('expression')!==-1)) {
            content = content[1];
            i += content.length - 1;
            tokens.push([content, 'regex']);
            modeStack.push('variable');
          } else {
            if (contents[i + 1] === '=') {
              tokens.push([contents.substring(i, i + 2), 'op']);
              i++;
            }
            tokens.push([contents[i], 'op']);
          }
        }
      } else if (/^instanceof/.test(sub)) {
        tokens.push(['instanceof', 'op']);
        i += 9;
      } else if (/^typeof/.test(sub)) {
        tokens.push(['typeof', 'op']);
        i += 5;
      } else if (/^((===)|(\!==)|(>>>))$/.test(contents.substring(i, i + 3))) {
        tokens.push([contents.substring(i, i + 3), 'op']);
        i += 2;
      } else if (/[+\-*\/%!><=]/.test(contents[i]) && contents[i + 1] === '=') {
        tokens.push([contents.substring(i, i + 2), 'op']);
        i++;
      } else if (/^((\+\+)|(--)|(&&)|(\|\|)|(<<)|(>>))$/.test(contents.substring(i, i + 2))) {
        tokens.push([contents.substring(i, i + 2), 'op']);
        i++;
      } else if (/[+\-\/\*%!&|<>=?~^;:]/.test(contents[i])) {
        tokens.push([contents[i], 'op']);
      } else if (contents[i] === ',') {
        tokens.push([',', 'op']);
        if (modeStack.length > 2 && (modeStack[modeStack.length - 2] === 'object_opened')) {
          modeStack.pop();
        }
      } else if (/[0-9.]/.test(contents[i])) {
        let content = sub.match(/^(([0-9]+(\.[0-9]+)?)|(\.[0-9]+))/)[1];
        tokens.push([content, 'number']);
        i += content.length - 1;
      } else if (contents[i] === '(') {
        tokens.push(['(', 'pare_expression_open']);
        modeStack.push('expression_opened');
      } else if (contents[i] === ')') {
        tokens.push([')', 'pare_expression_close']);
        modeStack.pop();
        modeStack.push('variable');
      } else if (contents[i] === '[') {
        tokens.push([']', 'arr_expression_open']);
        modeStack.push('expression_opened');
      } else if (contents[i] === ']') {
        tokens.push([']', 'arr_expression_close']);
        modeStack.pop();
        modeStack.push('variable');
      } else if (contents[i] === '{') {
        tokens.push(['{', 'object_expression_open']);
        modeStack.push('object_opened');
      } else if (contents[i] === '}') {
        if (modeStack[modeStack.length - 2] === 'object_opened') {
          tokens.push(['}', 'object_expression_close']);
        } else if (modeStack[modeStack.length - 2] === 'function_body_opened') {
          tokens.push(['}', 'function_body_close']);
          modeStack.pop();
        }
        modeStack.pop();
        modeStack.pop();
        modeStack.push('variable');
      } else if (/^function/.test(sub)) {
        tokens.push(['function', 'function_keyword']);
        modeStack.push('function_declaration');
        i += 7;
      } else {
        let l = keywords.length;
        let found = false;
        if (/[a-zA-Z_$]/.test(contents[i])) {
          let tst = sub.match(/^([a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
          for (let x = 0; x < l; x++) {
            if (tst === keywords[x]) {
              found = true;
              tokens.push([keywords[x], 'keyword']);
              i += keywords[x].length - 1;
              modeStack.push('variable');
              break;
            }
          }
          if (!found) {
            let content = tst;
            tokens.push([content, 'variable']);
            i += content.length - 1;
            modeStack.push('variable');
          }
        }
      }
    } else if (mode === 'variable') {
      if (contents[i] === '.') {
        let content = sub.match(/^([.][a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
        tokens.push([content, 'sub_variable']);
        i += content.length - 1;
      } else if (contents[i] === '?' && contents[i + 1] === '.') {
        let content = sub.match(/^([\?][.]([a-zA-Z_$][a-zA-Z_$0-9]*))?/)[1];
        tokens.push([content, 'sub_variable_optional']);
        i += content.length - 1;
      } else if (contents[i] === '(') {
        tokens.push(['(', 'sub_pare_expression_open']);
        modeStack.push('expression_opened');
      } else if (contents[i] === '[') {
        tokens.push(['[', 'sub_arr_expression_open']);
        modeStack.push('expression_opened');
      } else {
        i--;
        while (modeStack[modeStack.length - 1] === 'variable') modeStack.pop();
      }
    } else if (mode === 'object_opened') {
      if (/[a-zA-Z_$]/.test(contents[i])) {
        let content = sub.match(/^([a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
        tokens.push([content, 'object_parameter_name']);
        i += content.length - 1;
      } else if (contents[i] === '"') {
        let content = sub.match(/^"(([^"\uFFFF]|\uFFFF"|\uFFFF[^"])+)/)[1];
        i += content.length + 1;
        content = content.replace(/(\uFFFF)?'/g, "'").replace(/(\uFFFF)?"/, '\uFFFF"');
        tokens.push([content, 'object_string_parameter']);
      } else if (contents[i] === "'") {
        let content = sub.match(/^'(([^'\uFFFF]|\uFFFF'|\uFFFF[^'])+)/)[1];
        i += content.length + 1;
        content = content.replace(/(\uFFFF)?'/g, "'").replace(/(\uFFFF)?"/, '\uFFFF"');
        tokens.push([content, 'object_string_parameter']);
      } else if (contents[i] === "}") {
        tokens.push(['}', 'object_expression_close']);
        modeStack.pop();
      } else if (contents[i] === ':') {
        tokens.push([':', 'object_assignment']);
        modeStack.push('expression_opened');
      }
    } else if (mode === 'function_declaration') {
      if (/[a-zA-Z_$]/.test(contents[i])) {
        let content = sub.match(/^([a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
        tokens.push([content, 'function_name']);
        i += content.length - 1;
      } else if (contents[i] === '(') {
        tokens.push(['(', 'parameter_list_open']);
        modeStack.push('parameter_list_opened');
      } else if (contents[i] === '{') {
        tokens.push(['{', 'function_body_open']);
        modeStack.push('function_body_opened');
        modeStack.push('expression_opened');
      }
    } else if (mode === 'parameter_list_opened') {
      if (/[a-zA-Z_$]/.test(contents[i])) {
        let content = sub.match(/^([a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
        tokens.push([content, 'parameter_name']);
        i += content.length - 1;
      } else if (contents[i] === ',') {
        tokens.push([',', 'op']);
      } else if (contents[i] === ')') {
        tokens.push([')', 'parameter_list_close']);
        modeStack.pop();
      }
    } else if (mode === 'if_opened' || mode === 'elseif_opened') {
      if (contents[i] === '(') {
        tokens.push(['(', 'pare_expression_open']);
        modeStack.push('expression_opened');
      } else {
        i--;
        modeStack.pop();
      }
    } else if (mode === 'each_opened') {
      if (contents[i] === '(') {
        tokens.push(['(', 'pare_expression_open']);
      } else if (/^in[^a-zA-Z_$][^]*/.test(sub)) {
        tokens.push(['in', 'keyword']);
        modeStack.push('expression_opened');
        i++;
      } else if (/[a-zA-Z_$]/.test(contents[i])) {
        let content = sub.match(/^([a-zA-Z_$][a-zA-Z_$0-9]*)/)[1];
        tokens.push([content, 'keyword']);
        i += content.length - 1;
      } else if (contents[i - 1] === ')') {
        modeStack.pop();
        i--;
      }
    }
  }
  return tokens;
}

function proto_ast_maker(tokens) {
  let l = tokens.length;
  let out = {
    type: 'tag',
    name: 'html',
    children: []
  }
  let node = out;
  let parentPath = [];

  function handle_expression(token, type) {
    let handled = false;
    if (token[1] === 'string') {
      let newnode = {
        type: 'string',
        content: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'regex') {
      let newnode = {
        type: 'regex',
        content: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'number') {
      let newnode = {
        type: 'number',
        content: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'op') {
      let newnode = {
        type: 'op',
        op: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'keyword') {
      let newnode = {
        type: 'keyword',
        content: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'function_keyword') {
      let newnode = {
        type: 'function_definition',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'variable') {
      let newnode = {
        type: 'variable',
        content: token[0]
      };
      node.children.push(newnode);
      handled = true;
    } else if (token[1] === 'sub_variable') {
      let newnode = {
        type: 'sub_variable',
        content: token[0]
      };
      node.children.push(newnode);
    } else if (token[1] === 'sub_variable_optional') {
      let newnode = {
        type: 'sub_variable_optional',
        content: token[0]
      };
      node.children.push(newnode);
    } else if (token[1] === 'pare_expression_open') {
      let newnode = {
        type: 'sub_expression',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'arr_expression_open') {
      let newnode = {
        type: 'array_definition',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'object_expression_open') {
      let newnode = {
        type: 'object_definition',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'sub_pare_expression_open') {
      let newnode = {
        type: 'call_parameters',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'sub_arr_expression_open') {
      let newnode = {
        type: 'array_access',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
      handled = true;
    } else if (token[1] === 'expression_close') {
      node = parentPath.pop();
      handled = true;
    }
    return handled;
  }

  function handle_html(token, type, i) {
    if (token[1] === 'tag_open' && tokens[i + 1][1] !== 'tag_closer') {
      let newnode = {
        type: 'tag',
        children: [],
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'tag_open' && tokens[i + 1][1] === 'tag_closer') {
      node.closed = true;
    } else if (token[1] === 'tag_identifier') {
      if (node.closed !== true) {
        node.name = token[0];
        if (node.name in selfClosingTags) {
          node.self_close = true;
          node.closed = true;
        }
      }
    } else if (token[1] === 'tag_parameter') {
      let newnode = {
        type: 'tag_parameter',
        name: token[0],
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'tag_close') {
      if (node.closed && (tokens.length <= i + 3 || tokens[i + 1][1] != 'tag_open' || tokens[i + 2][1] != 'tag_closer' || tokens[i + 3][0] != node.name)) {
        delete node.closed;
        if (node.self_close) {
          let newnode = {
            type: 'tag_close',
            children: []
          };
          node.children.push(newnode);
        }
        node = parentPath.pop();
      } else {
        let newnode = {
          type: 'tag_close',
          children: []
        };
        node.children.push(newnode);
      }
    } else if (token[1] === 'doctype') {
      let newnode = {
        type: 'doctype',
        content: token[0],
        children: []
      };
      node.children.push(newnode);
    } else if (token[1] === 'html_content' || token[1] === 'html_comment') {
      let newnode = {
        type: 'html_content',
        content: token[0]
      };
      node.children.push(newnode);
    } else if (token[1] === 'javascript_content') {
      let newnode = {
        type: 'javascript_content',
        content: token[0]
      };
      node.children.push(newnode);
    } else if (token[1] === 'css_content') {
      let newnode = {
        type: 'css_content',
        content: token[0]
      };
      node.children.push(newnode);
    } else if (token[1] === 'clean_expression_open') {
      let newnode = {
        type: 'clean_expression',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'json_expression_open') {
      let newnode = {
        type: 'json_expression',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'expression_open') {
      let newnode = {
        type: 'expression',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'if_open') {
      let newnode = {
        type: 'if',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'elseif_open') {
      let newnode = {
        type: 'elseif',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'else_open') {
      let newnode = {
        type: 'else',
        children: []
      };
      node.children.push(newnode);
    } else if (token[1] === 'each_open') {
      let newnode = {
        type: 'each',
        children: []
      };
      parentPath.push(node);
      node.children.push(newnode);
      node = newnode;
    } else if (token[1] === 'if_close') {
      let newnode = {
        type: 'endif',
        children: []
      };
      node.children.push(newnode);
    } else if (token[1] === 'each_close') {
      let newnode = {
        type: 'endeach',
        children: []
      };
      node.children.push(newnode);
    }
  }
  for (let i = 0; i < l; i++) {
    let type = node.type;
    let token = tokens[i];
    if (type === 'tag') {
      handle_html(token, type, i)
    } else if (type === 'tag_parameter') {
      if (token[1] === 'tag_attribute_content') {
        let newnode = {
          type: 'tag_attribute_content',
          content: token[0]
        };
        node.children.push(newnode);
      } else if (token[1] === 'tag_single_attribute_content') {
        let newnode = {
          type: 'tag_single_attribute_content',
          content: token[0]
        };
        node.children.push(newnode);
      } else if (token[1] === 'clean_expression_open') {
        let newnode = {
          type: 'clean_expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else if (token[1] === 'expression_open') {
        let newnode = {
          type: 'expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else if (token[1] === 'tag_attribute_close') {
        node = parentPath.pop();
      } else if (token[1] === 'tag_single_attribute_close') {
        node = parentPath.pop();
      }
    } else if (type === 'if') {
      if (token[1] === 'pare_expression_open') {
        let newnode = {
          type: 'sub_expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else {
        node = parentPath.pop();
      }
    } else if (type === 'elseif') {
      if (token[1] === 'pare_expression_open') {
        let newnode = {
          type: 'sub_expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else {
        node = parentPath.pop();
      }
    } else if (type === 'else') {
      if (token[1] === 'pare_expression_open') {
        let newnode = {
          type: 'sub_expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else {
        node = parentPath.pop();
      }
    } else if (type === 'each') {
      if (token[1] === 'pare_expression_open') {} else if (token[1] === 'keyword' && token[0] !== 'in') {
        node.content = token[0];
      } else if (token[1] === 'keyword' && token[0] === 'in') {
        let newnode = {
          type: 'each_expression',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else {
        node = parentPath.pop();
        i--;
      }
    } else if (type === 'each_expression') {
      if (token[1] === 'pare_expression_close') {
        node = parentPath.pop();
      } else {
        handle_expression(token, type);
      }
    } else if (type === 'clean_expression' || type === 'expression' || type === "json_expression") {
      handle_expression(token, type);
    } else if (type === 'call_parameters' || type === 'sub_expression') {
      if (token[1] === 'pare_expression_close') {
        node = parentPath.pop();
      } else {
        handle_expression(token, type);
      }
    } else if (type === 'array_access' || type === 'array_definition') {
      if (token[1] === 'arr_expression_close') {
        node = parentPath.pop();
      } else {
        handle_expression(token, type);
      }
    } else if (type === 'object_definition') {
      if (token[1] === 'object_parameter_name') {
        let newnode = {
          type: 'object_parameter_name',
          content: token[0],
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
        handled = true;
      } else if (token[1] === 'object_string_parameter') {
        let newnode = {
          type: 'object_string_parameter',
          content: token[0],
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
        handled = true;
      }
      else if (token[1] === 'object_expression_close') {
        node = parentPath.pop();
        node = parentPath.pop();
      }
      else if (token[1] !== 'object_assignment'){
        let newnode = {
          type: 'object_expression',
          content: token[0],
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
        handled = true;
        i--;
      }
    } else if (type === 'object_parameter_name' || type === 'object_string_parameter' || type === 'object_expression') {
      if (token[1] === 'object_expression_close') {
        node = parentPath.pop();
        node = parentPath.pop();
      } else if (token[1] === 'op' && token[0] === ',') {
        node = parentPath.pop();
      } else if (token[1] === 'object_assignment' && (type === 'object_parameter_name' || type === 'object_string_parameter')) {
        node = parentPath.pop();
      } else {
        handle_expression(token, type);
      }
    } else if (type === 'function_definition') {
      if (token[1] === 'function_name') {
        node.name = token[0];
      } else if (token[1] === 'parameter_list_open') {
        let newnode = {
          type: 'function_parameters',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      } else if (token[1] === 'function_body_open') {
        let newnode = {
          type: 'function_body',
          children: []
        };
        parentPath.push(node);
        node.children.push(newnode);
        node = newnode;
      }
    } else if (type === 'function_parameters') {
      if (token[1] === 'parameter_name') {
        let newnode = {
          type: 'parameter_name',
          content: token[0]
        };
        node.children.push(newnode);
      } else if (token[1] === 'op') {
        let newnode = {
          type: 'op',
          op: ','
        };
        node.children.push(newnode);
      } else if (token[1] === 'parameter_list_close') {
        node = parentPath.pop();
      }
    } else if (type === 'function_body') {
      if (token[1] === 'function_body_close') {
        node = parentPath.pop();
        node = parentPath.pop();
      } else {
        handle_expression(token, type);
      }
    }
  }
  out.type = 'html';
  delete out.name;
  let object_expr = ast_finder(out,[
    ['type', 'object_expression']
  ],true,false);
  object_expr.forEach(function(ast_result){
    ast_result.node.type = 'sub_expression';
  });
  return out;
}

function ast_finder(node, conditions, multiple, recursive, path) {
  if (typeof path === 'undefined') {
    path = [];
  }
  let out = [];
  let kl = conditions.length;
  if (node.children) {
    let l = node.children.length;
    for (let i = 0; i < l; i++) {
      let useNode = node.children[i];
      let match = true;
      for (let x = 0; x < kl; x++) {
        if (useNode[conditions[x][0]] !== conditions[x][1]) {
          match = false;
          break;
        }
      }
      if (!multiple && out.length) {
        break;
      } else if ((match && recursive) || (!match)) {
        if (match) {
          out.push({
            node: useNode,
            path: path.concat([{
              node: node,
              index: i
            }])
          });
        }
        out.splice(out.splice.length, 0, ...ast_finder(useNode, conditions, multiple, recursive, path.concat([{
          node: node,
          index: i
        }])));
      } else if (match) {
        out.push({
          node: useNode,
          path: path.concat([{
            node: node,
            index: i
          }])
        });
      }
    }
  }
  return multiple ? out : out.slice(0, 1);
}

function ast_tag_objectify(tag) {
  let out = {
    _name: tag.name
  }
  let l = tag.children.length;
  for (let i = 0; i < l; i++) {
    let child = tag.children[i];
    if (child.type === 'tag_parameter') {
      out[child.name] = child.children.reduce((prev, cur) => {
        return prev + cur.content;
      }, '');
    } else if (child.type === 'scoping') {
      out._scoping = child.children.reduce((prev, cur) => {
        return prev + cur.content;
      }, '');
    }
  }
  return out;
}

function block_compile(ast, inside_func) {
  let node = ast;
  let out = '';
  let eachstack = [];
  let prevNode = undefined;
  let prevCount = 0;
  while (node) {
    if (node === prevNode) {
      prevCount++;
      if (prevCount > 100) {
        throw new Error('Infinite Loop Detected');
      }
    } else {
      prevNode = node;
      prevCount = 0;
    }
    let children = (node.children || []).filter((child) => {
      return (typeof child.parent === 'undefined');
    });
    let l = children.length;
    if (node.type === 'html') {
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        node = undefined;
      }
    } else if (node.type === 'doctype') {
      out += node.content;
      node = node.parent;
    } else if (node.type === 'tag') {
      if (l === node.children.length && node.name !== 'NOOP') {
        if (node.name !== 'stack') {
          out += '<' + node.name;
        }
      }
      if (l) {
        if (node.name === 'stack' && l === node.children.length) {
          let tnodes = node.children.filter(function (child) {
            return child.type === 'tag_parameter' || child.type === 'tag_close'
          });
          tnodes.forEach(function (child) {
            child.parent = node;
          });

        } else {
          children[0].parent = node;
          node = children[0];
        }
      } else {
        if (node.self_close !== true && node.name !== 'stack' && node.name !== 'NOOP') {
          out += '</' + node.name + '>';
        }
        node = node.parent;
      }
    } else if (node.type === 'tag_close') {
      if (node.parent.name !== 'stack') {
        out += '>';
      }
      node = node.parent;
    } else if (node.type === 'html_content') {
      out += node.content.replace(/[\n\r]+/g, "\\n'+\n'");
      node = node.parent;
    } else if (node.type === 'javascript_content') {
      out += node.content.replace(/'/g, "\\'").replace(/[\n\r]+/g, "\\n'+\n'");
      node = node.parent;
    } else if (node.type === 'css_content') {
      out += node.content.replace(/'/g, "\\'").replace(/[\n\r]+/g, "\\n'+\n'");
      node = node.parent;
    } else if (node.type === 'tag_parameter') {
      if (l === node.children.length) {
        out += ' ' + node.name + (node.children.length ? '="' : '');
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += (node.children.length ? '"' : '');
        node = node.parent;
      }
    } else if (node.type === 'scoping') {
      if (l === node.children.length) {
        out += ' ' + node.name + (node.children.length ? '="' : '');
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += (node.children.length ? '"' : '');
        node = node.parent;
      }
    } else if (node.type === 'tag_attribute_content') {
      out += node.content.replace(/'/g, "\\'").replace(/"/g,'\\"');
      node = node.parent;
    } else if (node.type === 'tag_single_attribute_content') {
      out += node.content.replace(/"/g, '\\"').replace(/'/g, "\\'");
      node = node.parent;
    } else if (node.type === 'component') {
      if (l === node.children.length) {
        out += "';(function";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ";out+='";
        node = node.parent;
      }
    } else if (node.type === 'component_variables') {
      if (l === node.children.length) {
        out += '(';
      }
      if (l) {
        if (l !== node.children.length) {
          out += ',';
        }
        children[0].parent = node;
        node = children[0];
      } else {
        out += "){out+='";
        node = node.parent;
      }
    } else if (node.type === 'component_var') {
      out += node.content;
      node = node.parent;
    } else if (node.type === 'component_call') {
      if (l === node.children.length) {
        out += "';})(";
      }
      if (l) {
        if (l !== node.children.length) {
          out += ',';
        }
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")";
        node = node.parent;
      }
    } else if (node.type === 'if') {
      if (l === node.children.length) {
        out += "';if";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        if (node.closed !== true) {
          out += '{out+=\'';
        }
        node = node.parent;
      }
    } else if (node.type === 'elseif') {
      if (l === node.children.length) {
        out += "';}else if";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        if (node.closed !== true) {
          out += '{out+=\'';
        }
        node = node.parent;
      }
    } else if (node.type === 'else') {
      if (l === node.children.length) {
        out += "';}else";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        if (node.closed !== true) {
          out += '{out+=\'';
        }
        node = node.parent;
      }
    } else if (node.type === 'endif') {
      out += "';}out+='";
      node = node.parent;
    } else if (node.type === 'each') {
      if (l === node.children.length) {
        out += `';arrayify`;
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += '.forEach(function(' + node.content + '){out+=\'';
        eachstack.push(node.content);
        node = node.parent;
      }
    } else if (node.type === 'each_expression') {
      if (l === node.children.length) {
        out += "(";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")";
        node = node.parent;
      }
    } else if (node.type === 'endeach') {
      out += "';});out+='";
      eachstack.pop();
      node = node.parent;
    } else if (node.type === 'json_expression') {
      if (l === node.children.length) {
        out += "'+_json(";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")+'";
        node = node.parent;
      }
    } else if (node.type === 'clean_expression') {
      if (l === node.children.length) {
        out += "'+_clean(";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")+'";
        node = node.parent;
      }
    } else if (node.type === 'expression') {
      if (l === node.children.length) {
        out += "'+(";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")+'";
        node = node.parent;
      }
    } else if (node.type === 'string') {
      out += '"' + node.content.replace(/\uffff/,'\\') + '"';
      node = node.parent;
    } else if (node.type === 'regex') {
      out += node.content.replace(/\uffff/,'\\');
      node = node.parent;
    } else if (node.type === 'op') {
      out += ' ' + node.op + ' ';
      node = node.parent;
    } else if (node.type === 'number') {
      out += ' ' + node.content;
      node = node.parent;
    } else if (node.type === 'keyword') {
      out += ' ' + node.content;
      node = node.parent;
    } else if (node.type === 'sub_variable') {
      out += node.content.replace(/\uffff/,'\\');
      node = node.parent;
    } else if (node.type === 'sub_variable_optional') {
      out += node.content.replace(/\uffff/,'\\');
      node = node.parent;
    } else if (node.type === 'call_parameters') {
      if (l === node.children.length) {
        out += "(";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ")";
        node = node.parent;
      }
    } else if (node.type === 'array_access') {
      if (l === node.children.length) {
        out += "[";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += "]";
        node = node.parent;
      }
    } else if (node.type === 'array_definition') {
      if (l === node.children.length) {
        out += "[";
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += "]";
        node = node.parent;
      }
    } else if (node.type === 'object_definition') {
      if (l === node.children.length) {
        out += "{";
      }
      if (l) {
        if(l !== node.children.length && (children[0].type === 'object_string_parameter' || children[0].type === 'object_parameter_name')){
          out+=',';
        }
        children[0].parent = node;
        node = children[0];
      } else {
        out += "}";
        node = node.parent;
      }
    } else if (node.type === 'object_parameter_name') {
      out += node.content + ':';
      node = node.parent;
    } else if (node.type === 'object_string_parameter') {
      out += '"' + node.content.replace(/\uFFFF/g,'\\') + '":';
      node = node.parent;
    } else if (node.type === 'sub_expression') {
      if (l === node.children.length) {
        out += '(';
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ')';
        node = node.parent;
      }
    } else if (node.type === 'function_definition') {
      if (l === node.children.length) {
        out += ' function' + (node.name ? ' ' + node.name : '');
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        node = node.parent;
      }
    } else if (node.type === 'function_parameters') {
      if (l === node.children.length) {
        out += '(';
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += ')'
        node = node.parent;
      }
    } else if (node.type === 'parameter_name') {
      out += node.content;
      node = node.parent;
    } else if (node.type === 'function_body') {
      if (l === node.children.length) {
        out += '{';
        inside_func++;
      }
      if (l) {
        children[0].parent = node;
        node = children[0];
      } else {
        out += '}'
        node = node.parent;
        inside_func--;
      }
    } else if (node.type === 'variable') {
      if (inside_func || (eachstack.length > 0 && eachstack.indexOf(node.content) !== -1)) {
        out += ' ' + node.content;
      } else {
        out += ' _vars("' + node.content + '")';
      }
      node = node.parent;
    }
  }
  out = out.replace(/\uFFFF/g,'\\\\');
  return out;
}

function compile_file(sourceFile, ast, callback, client) {
  let varStr = 'let';
  if (client) {
    varStr = 'var';
  }
  let funcstr = '';
  let func = undefined;
  try {
    funcstr = `
      ${varStr} _sourceName = '${sourceFile}';
      ${varStr} out = '',i,l,arr,eachstack = [];
      function _clean(val){
        val = val + "";
        return val.replace(/&/g,\'&amp;\').replace(/</g,\'&lt;\').replace(/\'/g,\'&apos;\').replace(/"/g,\'&quot;\').replace(/>/g,\'&gt;\');
      }
      function _json(val){
        return JSON.stringify(val).replace(/'/g,"\\\\\\'").replace(/\\\\"/g,'\\\\\\\\"').replace(/\\n/g,'\\\\n').replace(/\\\\n/g,'\\\\\\\\n').replace(/\\r/g,'\\\\r').replace(/\\\\r/g,'\\\\\\\\r');
      }
      function _vars(name){
        ${client ? `if(name==='_class') name='class';` : ''}
        if(name in options){
          return options[name];
        }
        else if(name in ${client?'window':'global'}){
          return ${client?'window':'global'}[name];
        }
        else{
          return undefined;
        }
      }
      function arrayify(expression,save_keys){
        var type = (typeof expression);
        if(Array.isArray(expression)){
          return expression;
        }
        else if(type === 'string' || expression instanceof String){
          return expression.split('');
        }
        else if(type === 'number' || expression instanceof Number){
          return new Array(expression).fill(0);
        }
        else if (save_keys){
          return Object.keys(expression).map(function(inp){return [inp,expression[inp]];});
        }
        else{
          return Object.keys(expression).map(function(inp){return expression[inp];});
        }
      }
      ${client?'if(typeof options._data === "undefined") options._data = {};':''}
      try{
        out = '`;
  let compiled = block_compile(ast, 0);
  funcstr += compiled + `';
        ${client?'var tempParent = document.createElement("template"); tempParent.innerHTML=out; return tempParent.content.cloneNode(true).children[0]':'callback(null,out)'};
      }
      catch(err){
        ${client?"throw (new Error('error in View File '+_sourceName+' \\\\n  '+err))":"callback(new Error('error in View File '+_sourceName+' \\\\n  '+err))"};
      }`;
    funcstr = funcstr.replace(/\uFFFF/g,"\\\\");
    func = new Function('options', 'callback', funcstr);
  } catch (e) {
    options.error(e);
    if (options.debug) {
      options.log(funcstr);
    }
    return callback(e);
  }
  if (client) {
    return func;
  } else {
    callback(null, func);
  }
}

async function inherit_tree(filepath, components, content, input_ast) {
  let contents = filepath !== '' ? await fs.readFile(filepath, 'utf8') : content;
  let tokens = lexer(contents);
  let ast = (typeof input_ast !== 'undefined' ? input_ast : proto_ast_maker(tokens));
  let out = ast;
  out.components = components;
  out.requiredComps = [];
  out.requiredScripts = [];
  out.requiredStylesheets = [];
  out.compConstructors = [];
  if (ast.children.length) {
    let requires = ast_finder(ast, [
      ['type', 'tag'],
      ['name', 'require']
    ], true, false);
    if (requires.length) {
      let l = requires.length;
      for (let i = l - 1; i >= 0; i--) {
        let tag = ast_tag_objectify(requires[i].node);
        if (tag.stylesheet) {
          // add required stylesheet to the front of the list
          out.requiredStylesheets.unshift(tag.stylesheet);
        } else if (tag.script) {
          // add required script to the front of the list
          out.requiredScripts.unshift(tag.script);
        } else {
          let filepath = tag.path;
          let componentName = tag.name;
          let componentVersion = tag.version || tag.v || 'latest';
          let compName = componentName || filepath.match(/([^\\\/]+)([\.][^]+)?$/)[1];
          if (!(compName in components)) {
            components[compName] = {};
          }
          out.requiredComps.unshift(compName + '---' + componentVersion);
          if (!(componentVersion in components[compName])) {
            components[compName][componentVersion] = 'standin';
            let componentContent = await loadComponent(filepath, componentName, componentVersion);
            let component = await inherit_tree('', components, componentContent);
            if (component.requiredComps) {
              out.requiredComps = out.requiredComps.concat(component.requiredComps.filter((input) => {
                return (out.requiredComps.indexOf(input) !== -1) ? false : true;
              }));
            }
            if (component.compConstructors) {
              out.compConstructors = out.compConstructors.concat(component.compConstructors.filter((input) => {
                return (out.compConstructors.indexOf(input) !== -1) ? false : true;
              }));
            }
            if (component.requiredStylesheets) {
              out.requiredStylesheets = out.requiredStylesheets.concat(component.requiredStylesheets.filter((input) => {
                return (out.requiredStylesheets.indexOf(input) !== -1) ? false : true;
              }));
            }
            if (component.requiredScripts) {
              out.requiredScripts = out.requiredScripts.concat(component.requiredScripts.filter((input) => {
                return (out.requiredScripts.indexOf(input) !== -1) ? false : true;
              }))
            }
            let scoping = ast_finder(component, [
              ['type', 'tag_parameter'],
              ['name', 'aleet-scoped']
            ], true, true);
            if (scoping.length > 0) {
              for (let i = 0; i < scoping.length; i++) {
                let scoped = scoping[i];
                let pop = {
                  type: undefined
                };
                let path_index = scoped.path.length;
                while (pop.type !== 'tag' && pop.type !== 'component' && path_index > 0) {
                  path_index--;
                  pop = scoped.path[path_index].node;
                }
                let tag_obj = ast_tag_objectify(pop);
                if (tag_obj._scoping) {
                  let attr = ast_finder(pop, [
                    ['type', 'scoping']
                  ])[0];
                  attr.node.children.push({
                    type: 'tag_attribute_content',
                    content: ' ' + compName + '---' + componentVersion,
                  });
                  scoped.path[scoped.path.length - 1].node.children.splice(scoped.path[scoped.path.length - 1].index, 1);
                } else {
                  scoped.node.children.push({
                    type: 'tag_attribute_content',
                    content: compName + '---' + componentVersion,
                  })
                  scoped.node.type = 'scoping';
                }
              }
            }
            components[compName][componentVersion] = {
              ast: component
            };
            components._list.push(compName + '---' + componentVersion);
          } else if(components[compName][componentVersion] !== 'standin') {
            let component = components[compName][componentVersion].ast;
            if (component.requiredComps) {
              out.requiredComps = out.requiredComps.concat(component.requiredComps.filter((input) => {
                return (out.requiredComps.indexOf(input) !== -1) ? false : true
              }));
            }
            if (component.compConstructors) {
              out.compConstructors = out.compConstructors.concat(component.compConstructors.filter((input) => {
                return (out.compConstructors.indexOf(input) !== -1) ? false : true
              }));
            }
            if (component.requiredStylesheets) {
              out.requiredStylesheets = out.requiredStylesheets.concat(component.requiredStylesheets.filter((input) => {
                return (out.requiredStylesheets.indexOf(input) !== -1) ? false : true;
              }));
            }
            if (component.requiredScripts) {
              out.requiredScripts = out.requiredScripts.concat(component.requiredScripts.filter((input) => {
                return (out.requiredScripts.indexOf(input) !== -1) ? false : true;
              }))
            }
          }
          let rNode = requires[i].path[requires[i].path.length - 1];
          rNode.node.children.splice(rNode.index, 1);
        }
      }
    }

    let deferred = ast_finder(ast, [
      ['type', 'tag'],
      ['name', 'defer']
    ], true, false);
    if (deferred.length) {
      let l = deferred.length;
      for (let i = l - 1; i >= 0; i--) {
        let tag = ast_tag_objectify(deferred[i].node);
        let filepath = tag.path;
        let componentName = tag.name;
        let componentVersion = tag.version || tag.v || 'latest';
        let compName = componentName || filepath.match(/([^\\\/]+)([\.][^]+)?$/)[1];
        if (!(compName in components)) {
          components[compName] = {};
        }
        if (!(componentVersion in components[compName])) {
          components[compName][componentVersion] = 'standin';
          let componentContent = await loadComponent(filepath, componentName, componentVersion);
          await inherit_tree('', components, componentContent);
        }
        let rNode = deferred[i].path[deferred[i].path.length - 1];
        rNode.node.children.splice(rNode.index, 1);
      }
    }

    let constructors = ast_finder(ast, [
      ['type', 'tag'],
      ['name', 'constructor']
    ], true, false);
    if (constructors.length) {
      let l = constructors.length;
      for (let i = l - 1; i >= 0; i--) {
        let tag = ast_tag_objectify(constructors[i].node);
        let filepath = tag.path;
        let componentName = tag.name;
        let componentVersion = tag.version || tag.v || 'latest';
        let compName = componentName || filepath.match(/([^\\\/]+)([\.][^]+)?$/)[1];
        if (!(compName in components)) {
          components[compName] = {};
        }
        out.compConstructors.push(compName + '---' + componentVersion);
        if (!(componentVersion in components[compName])) {
          let componentContent = await loadComponent(filepath, componentName, componentVersion);
          let component = await inherit_tree('', components, componentContent);
          if (component.requiredComps) {
            out.requiredComps = out.requiredComps.concat(component.requiredComps.filter((input) => {
              return (out.requiredComps.indexOf(input) !== -1) ? false : true
            }));
          }
          if (component.compConstructors) {
            out.compConstructors = out.compConstructors.concat(component.compConstructors.filter((input) => {
              return (out.compConstructors.indexOf(input) !== -1) ? false : true
            }));
          }
          if (component.requiredStylesheets) {
            out.requiredStylesheets = out.requiredStylesheets.concat(component.requiredStylesheets.filter((input) => {
              return (out.requiredStylesheets.indexOf(input) !== -1) ? false : true;
            }));
          }
          if (component.requiredScripts) {
            out.requiredScripts = out.requiredScripts.concat(component.requiredScripts.filter((input) => {
              return (out.requiredScripts.indexOf(input) !== -1) ? false : true;
            }))
          }
          components[compName][componentVersion] = {
            ast: component
          };
          components._list.push(compName + '---' + componentVersion);
        } else if(components[compName][componentVersion] !== 'standin') {
          let component = components[compName][componentVersion].ast;
          if (component.requiredComps) {
            out.requiredComps = out.requiredComps.concat(component.requiredComps.filter((input) => {
              return (out.requiredComps.indexOf(input) !== -1) ? false : true
            }));
          }
          if (component.compConstructors) {
            out.compConstructors = out.compConstructors.concat(component.compConstructors.filter((input) => {
              return (out.compConstructors.indexOf(input) !== -1) ? false : true
            }));
          }
          if (component.requiredStylesheets) {
            out.requiredStylesheets = out.requiredStylesheets.concat(component.requiredStylesheets.filter((input) => {
              return (out.requiredStylesheets.indexOf(input) !== -1) ? false : true;
            }));
          }
          if (component.requiredScripts) {
            out.requiredScripts = out.requiredScripts.concat(component.requiredScripts.filter((input) => {
              return (out.requiredScripts.indexOf(input) !== -1) ? false : true;
            }))
          }
        }
        let rNode = constructors[i].path[constructors[i].path.length - 1];
        rNode.node.children.splice(rNode.index, 1);
      }
    }

    if (ast.children[0].type === 'tag' && ast.children[0].name === 'inherits') {
      let filename = ast_finder(ast.children[0], [
        ['type', 'tag_parameter'],
        ['name', 'name']
      ], false)[0].node.children.reduce(function (returning, child) {
        return returning += '' + child.content;
      }, '');
      if (path.extname(filename) === "") {
        filename += '.html';
      }
      let newpath = path.join(options.viewPath, filename);
      let parent = await inherit_tree(newpath, components);
      let parentSubs = ast_finder(parent, [
        ['type', 'tag'],
        ['name', 'sub']
      ], true, false);
      let mySubs = ast_finder(ast, [
        ['type', 'tag'],
        ['name', 'sub']
      ], true, false);
      let parentStacks = ast_finder(parent, [
        ['type', 'tag'],
        ['name', 'stack']
      ], true, false);
      let myStacks = ast_finder(ast, [
        ['type', 'tag'],
        ['name', 'stack']
      ], true, false);
      if (parentSubs) {
        let l = parentSubs.length;
        for (let i = l - 1; i >= 0; i--) {
          let pSub = parentSubs[i];
          let pName = ast_finder(pSub.node, [
            ['type', 'tag_parameter'],
            ['name', 'name']
          ], false, false)[0].node.children.reduce((returning, child) => {
            return returning += child.content
          }, '');
          let m = mySubs.length;
          for (let x = 0; x < m; x++) {
            let mSub = mySubs[x];
            let mClose = ast_finder(mSub.node, [
              ['type', 'tag_close']
            ], false, false)[0];
            if (mClose) {
              let mcPath = mClose.path[mClose.path.length - 1];
              mcPath.node.children.splice(mcPath.index, 1);
            }
            let mName = ast_finder(mSub.node, [
              ['type', 'tag_parameter'],
              ['name', 'name']
            ], false, false)[0].node.children.reduce((returning, child) => {
              return returning += child.content
            }, '');
            if (mName === pName) {
              let pNode = pSub.path[pSub.path.length - 1];
              pNode.node.children.splice(pNode.index, 1, ...mSub.node.children.filter((child) => {
                return (child.type !== 'tag_parameter');
              }));
              break;
            }
          }
        }
      }
      if (parentStacks) {
        let l = parentStacks.length;
        for (let i = l - 1; i >= 0; i--) {
          let pStack = parentStacks[i];
          let pName = ast_finder(pStack.node, [
            ['type', 'tag_parameter'],
            ['name', 'name']
          ], false, false)[0].node.children.reduce((returning, child) => {
            return returning += child.content
          }, '');
          let m = myStacks.length;
          for (let x = 0; x < m; x++) {
            let mStack = myStacks[x];
            let mClose = ast_finder(mStack.node, [
              ['type', 'tag_close']
            ], false, false)[0];
            // if(mClose){
            //   let mcPath = mClose.path[mClose.path.length - 1];
            //   mcPath.node.children.splice(mcPath.index,1);
            // }
            let mName = ast_finder(mStack.node, [
              ['type', 'tag_parameter'],
              ['name', 'name']
            ], false, false)[0].node.children.reduce((returning, child) => {
              return returning += child.content
            }, '');
            if (mName === pName) {
              pStack.node.children.splice(pStack.node.children.length, 0, ...mStack.node.children.filter((child) => {
                return (child.type !== 'tag_parameter' && child.type !== 'tag_close');
              }));
              break;
            }
          }
        }
      }
      if (ast.requiredComps) {
        parent.requiredComps = parent.requiredComps.concat(ast.requiredComps);
      }
      if (ast.compConstructors) {
        parent.compConstructors = out.compConstructors.concat(ast.compConstructors);
      }
      if (ast.requiredStylesheets) {
        parent.requiredStylesheets = parent.requiredStylesheets.concat(ast.requiredStylesheets);
      }
      if (ast.requiredScripts) {
        parent.requiredScripts = parent.requiredScripts.concat(ast.requiredScripts);
      }
      ast = parent;
      out = ast;
    }

    let includes = ast_finder(ast, [
      ['type', 'tag'],
      ['name', 'include']
    ], true, false);
    if (includes.length) {
      let l = includes.length;
      for (let i = l - 1; i >= 0; i--) {
        let filename = ast_finder(includes[i].node, [
          ['type', 'tag_parameter'],
          ['name', 'name']
        ], false)[0].node.children.reduce(function (returning, child) {
          return returning += '' + child.content;
        }, '');
        if (path.extname(filename) === "") {
          filename += '.html';
        }
        let newpath = path.join(options.viewPath, filename);
        let include = await inherit_tree(newpath, components);
        if (include.requiredComps) {
          out.requiredComps = out.requiredComps.concat(include.requiredComps);
        }
        if (include.compConstructors) {
          out.compConstructors = out.compConstructors.concat(include.compConstructors);
        }
        if (include.requiredStylesheets) {
          out.requiredStylesheets = out.requiredStylesheets.concat(include.requiredStylesheets);
        }
        if (include.requiredScripts) {
          out.requiredScripts = out.requiredScripts.concat(include.requiredScripts);
        }
        let iNode = includes[i].path[includes[i].path.length - 1];
        iNode.node.children.splice(iNode.index, 1, ...include.children);
      }
    }

    for (let i = 0; i < components._list.length; i++) {
      let name_version = components._list[i];
      let name = name_version.match(/^([^]+)---/)[1];
      let version = name_version.match(/^[^]+---([^]+)/)[1];
      let comps = ast_finder(ast, [
        ['type', 'tag'],
        ['name', name]
      ], true, true);
      if (comps.length) {
        let l = comps.length;
        let template = ast_finder(components[name][version].ast, [
          ['type', 'tag'],
          ['name', 'template']
        ], false, false);
        let copy_from = ast_finder(template[0].node, [
          ['type', 'tag']
        ], false, false)[0];
        copy_from = object_copy(copy_from.node);
        let variables = ast_finder(copy_from, [
          ['type', 'variable']
        ], true, false);
        let vNodes = [];
        let vars = [];
        for (let y = 0; y < variables.length; y++) {
          let variable = variables[y];
          let include = true;
          for (let z = variable.path.length - 1; z > 0; z--) {
            let segment = variable.path[z];
            if (segment.node.type === 'component') {
              include = false;
              break;
            }
          }
          if (include || true) {
            if (vars.indexOf(variable.node.content) === -1) {
              vars.push(variable.node.content);
            }
            variable.node.type = 'keyword';
            vNodes.push(variable);
          }
        }
        // vnodes now contains all variables that are within this component, but not a sup-component
        vars = vars.filter((vr) => {
          return vr !== '_data'
        });
        vars.push('_data');
        for (let x = l - 1; x >= 0; x--) {
          let node = comps[x].node;
          let tag = ast_tag_objectify(node);
          let v = tag.version || tag.v || 'latest';
          if (v !== version) {
            continue;
          }
          let z = 0;
          let parameters = {
            _data: {}
          };
          let parent = comps[x].path[comps[x].path.length - 1];
          while (node.children.length > z && node.children[z].type === 'tag_parameter') {
            let param = node.children[z];
            let name = param.name.replace(/-/g, '_');
            if (name === 'class') name = '_class';
            if (vars.indexOf(name) !== -1) {
              let children = param.children;
              parameters[name] = [];
              for (let ci = 0; ci < children.length; ci++) {
                let child = children[ci];
                if (parameters[name].length > 0) {
                  parameters[name].push({
                    type: 'op',
                    op: '+'
                  });
                }
                if (child.type === 'tag_attribute_content' || child.type === 'tag_single_attribute_content') {
                  child.type = 'string';
                } else if (child.type === 'expression' || child.type === 'clean_expression') {
                  child.type = 'sub_expression';
                }
                parameters[name].push(child);
              }
            } else if (name.indexOf('data_') === 0) {
              let realName = name.replace(/^data_/, '');
              let children = param.children;
              parameters._data[realName] = [];
              for (let ci = 0; ci < children.length; ci++) {
                let child = children[ci];
                if (parameters._data[realName].length > 0) {
                  parameters._data[realName].push({
                    type: 'op',
                    op: '+'
                  });
                }
                if (child.type === 'tag_attribute_content') {
                  child.type = 'string';
                } else if (child.type === 'expression' || child.type === 'clean_expression') {
                  child.type = 'sub_expression';
                }
                parameters._data[realName].push(child);
              }
            }
            z++;
          }
          vars.forEach((input) => {
            if (input !== '_data') {
              if (typeof parameters[input] === 'undefined' || parameters[input].length === 0) {
                parameters[input] = [{
                  type: 'keyword',
                  content: 'undefined'
                }];
              }
            }
          });
          let copy = object_copy(copy_from);
          let tag_closing_bracket = ast_finder(copy, [
            ['type', 'tag_close']
          ], false, false)[0];
          let closePath = tag_closing_bracket.path[tag_closing_bracket.path.length - 1];
          //need to add data attributes to end of tag in copy
          var data_mirror = preforms.component_data_print();
          var data_keyword = ast_finder(data_mirror, [
            ['type', 'variable'],
            ['content', '_data']
          ], false, false)[0];
          data_keyword.node.type = 'keyword';
          let splice_index = closePath.index;
          if (typeof tag['aleet-scoped'] !== 'undefined') {
            closePath.node.children.splice(splice_index, 0, {
              type: 'tag_parameter',
              name: 'aleet-scoped',
              children: [{
                type: 'tag_attribute_content',
                content: tag['aleet-scoped']
              }]
            });
            splice_index++;
          }
          closePath.node.children.splice(splice_index, 0, data_mirror);
          //need to put whole thing under 'component' object
          copy = {
            type: 'tag',
            name: 'NOOP',
            children: [{
              type: 'component',
              children: [{
                  type: 'component_variables',
                  children: vars.map((child) => {
                    return {
                      type: 'component_var',
                      content: child,
                      children: []
                    }
                  })
                },
                copy,
                {
                  type: 'component_call',
                  children: vars.map((child) => {
                    if (child === '_data') {
                      return {
                        type: 'object_definition',
                        children: array_flat(Object.keys(parameters['_data']).map(function (key) {
                          return [{
                            type: 'object_string_parameter',
                            content: key.replace(/(\uFFFF)?"/g, '\uFFFF"').replace(/(\uFFFF)?'/g, "'"),
                            children: []
                          }].concat(parameters._data[key]);
                        }))
                      }
                    } else {
                      if (typeof parameters[child] !== 'undefined') {
                        return {
                          type: 'sub_expression',
                          children: parameters[child]
                        }
                      } else {
                        return {
                          type: 'keyword',
                          content: 'undefined',
                          children: []
                        }
                      }
                    }
                  })
                }
              ],
              parameters: object_copy(parameters),
              vars: vars.slice(0, vars.length)
            }]
          }
          let dest_slots = ast_finder(copy, [
            ['type', 'tag'],
            ['name', 'sl-t']
          ], true, false).concat(
            ast_finder(copy, [
              ['type', 'tag_parameter'],
              ['name', 'sl-t']
            ], true, false).map(function (param) {
              return {
                node: param.path[param.path.length - 1].node,
                path: param.path.slice(0, param.path.length - 1)
              }
            }));
          let src_slots = ast_finder(node, [
            ['type', 'tag'],
            ['name', 'sl-t']
          ], true, false);
          if (dest_slots.length > 0) {
            let src_slots_obj = {};
            let sl = src_slots.length;
            for (let si = sl - 1; si >= 0; si--) {
              let slot = ast_tag_objectify(src_slots[si].node);
              src_slots_obj[slot.name] = src_slots[si];
            }
            sl = dest_slots.length;
            for (let si = sl - 1; si >= 0; si--) {
              let slot = ast_tag_objectify(dest_slots[si].node);

              let name = slot.name;
              if (slot._name !== 'sl-t') {
                name = slot['sl-t'];
                ast_finder(dest_slots[si].node, [
                  ['type', 'tag_parameter'],
                  ['name', 'sl-t']
                ], true, false)[0].node.name += '--'
              } else {
                dest_slots[si].node.name += '--';
              }
              if (name in src_slots_obj) {
                let subin = src_slots_obj[name].node.children;
                while (subin[0].type !== 'tag_close') subin.shift();
                subin.shift();
                if (subin.some((input) => {
                    return (input.type !== 'html_content' || input.content.replace(/[ \r\n]/g, '').length > 0) ? true : false;
                  }) === true) {
                  dest_slots[si].node.children = dest_slots[si].node.children.concat(subin);
                }
              } else if (name === 'default') {
                let subin = node.children.slice();
                while (subin[0].type !== 'tag_close') subin.shift();
                subin.shift();
                subin = subin.filter((input) => {
                  return (input.type === 'tag' && input.name === 'sl-t') ? false : true;
                });
                if (subin.some((input) => {
                    return (input.type !== 'html_content' || input.content.replace(/[ \r\n]/g, '').length > 0) ? true : false;
                  }) === true) {
                  dest_slots[si].node.children = dest_slots[si].node.children.concat(subin);
                } else if (ast_tag_objectify(node)['sl-t']) {
                  const slotName = ast_tag_objectify(node)['sl-t'];
                  if (slot._name !== 'sl-t') {
                    const slotNode = ast_finder(dest_slots[si].node, [
                      ['type', 'tag_parameter'],
                      ['name', 'sl-t--']
                    ], true, false)[0].node;
                    slotNode.name = 'sl-t';
                    slotNode.children[0].content = slotName;
                  
                  } else {
                    dest_slots[si].node.name = 'sl-t';
                    ast_finder(dest_slots[si].node, [
                      ['type', 'tag_parameter'],
                      ['name', 'name']
                    ], true, false)[0].node.children[0].content = slotName;
                  }
                }
              } else {

              }
            }
          }
          //need to set up before and after component elements
          parent.node.children.splice(parent.index, 1, copy);
        }
      }
    }

    if (out.requiredComps.length || out.compConstructors.length || out.requiredScripts.length || out.requiredStylesheets.length) {
      let stacks = ast_finder(ast, [
        ['type', 'tag'],
        ['name', 'stack']
      ], true, false);
      let jsStack = undefined;
      let cssStack = undefined;
      let l = stacks.length;
      for (let i = 0; i < l; i++) {
        let name = ast_finder(stacks[i].node, [
          ['type', 'tag_parameter'],
          ['name', 'name']
        ], false, false)[0].node.children.reduce((returning, child) => {
          return returning += child.content
        }, '');
        if (name === 'components_css') {
          cssStack = stacks[i];
        } else if (name === 'components_js') {
          jsStack = stacks[i];
        }
      }

      if (cssStack && jsStack) {
        if(out.requiredStylesheets.length) {
          cssStack.node.children.push(...out.requiredStylesheets.map(link=>preforms.required_stylesheet(link)));
        }
        if(out.requiredScripts.length) {
          jsStack.node.children.push(...out.requiredScripts.map(link=>preforms.required_script(link)));
        }
        if (out.requiredComps.length) {
          cssStack.node.children.push(preforms.component_css_link(out.requiredComps));
          jsStack.node.children.push(preforms.component_js_script(out.requiredComps));
        }
        if (out.compConstructors.length) {
          jsStack.node.children.push(preforms.component_component_script(out.compConstructors));
        }
      }
    }

  }
  return out;
}

async function duplicate_component_removal(ast) {
  let remove = [];
  let stylesheets = {};
  let stylesheet_links = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'required_stylesheet']
  ], true);
  let l = stylesheet_links.length;
  for (let i = 0; i < l; i++) {
    let tag = ast_tag_objectify(stylesheet_links[i].node);
    let name = tag.href;
    if (name in stylesheets) {
      remove.push(stylesheet_links[i]);
    } else {
      stylesheets[name] = true;
    }
    stylesheet_links[i].node.name = 'link';
  }
  while (remove.length > 0) {
    let temp = remove.pop();
    let parentPath = temp.path[temp.path.length - 1];
    parentPath.node.children.splice(parentPath.index, 1);
  }

  let scripts = {};
  let script_links = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'required_script']
  ], true);
  l = script_links.length;
  for (let i = 0; i < l; i++) {
    let tag = ast_tag_objectify(script_links[i].node);
    let name = tag.src;
    if (name in scripts) {
      remove.push(script_links[i]);
    } else {
      scripts[name] = true;
    }
    script_links[i].node.name = 'script';
  }
  while (remove.length > 0) {
    let temp = remove.pop();
    let parentPath = temp.path[temp.path.length - 1];
    parentPath.node.children.splice(parentPath.index, 1);
  }

  let component_css = {};
  let component_links = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'component_link']
  ], true);
  l = component_links.length;
  for (let i = 0; i < l; i++) {
    let tag = ast_tag_objectify(component_links[i].node);
    let names = tag.href.replace(/\/components\/css\//, '').split('%7C');
    let nl = names.length;
    let sl = nl;
    for (let x = 0; x < nl; x++) {
      if (names[x] in component_css) {
        names.splice(x, 1);
        x--;
        nl--;
      } else {
        component_css[names[x]] = true;
      }
    }
    if (nl !== sl) {
      if (nl === 0) {
        remove.push(component_links[i]);
      } else {
        let newhref = '/components/css/' + names.join('%7C');
        let param = ast_finder(component_links[i].node, [
          ['type', 'tag_parameter'],
          ['name', 'href']
        ], false);
        param[0].node.children[0].content = newhref;
      }
    }
    component_links[i].node.name = 'link';
  }
  while (remove.length > 0) {
    let temp = remove.pop();
    let parentPath = temp.path[temp.path.length - 1];
    parentPath.node.children.splice(parentPath.index, 1);
  }

  let component_js = {};
  let component_scripts = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'component_script']
  ], true);
  l = component_scripts.length;
  for (let i = 0; i < l; i++) {
    let tag = ast_tag_objectify(component_scripts[i].node);
    let names = tag.src.replace(/\/components\/js\//, '').split('%7C');
    let nl = names.length;
    let sl = nl;
    for (let x = 0; x < nl; x++) {
      if (names[x] in component_js) {
        names.splice(x, 1);
        x--;
        nl--;
      } else {
        component_js[names[x]] = true;
      }
    }
    if (nl !== sl) {
      if (nl === 0) {
        remove.push(component_scripts[i]);
      } else {
        let newhref = '/components/js/' + names.join('%7C');
        let param = ast_finder(component_scripts[i].node, [
          ['type', 'tag_parameter'],
          ['name', 'src']
        ], false);
        param[0].node.children[0].content = newhref;
      }
    }
    component_scripts[i].node.name = 'script';
  }
  while (remove.length > 0) {
    let temp = remove.pop();
    let parentPath = temp.path[temp.path.length - 1];
    parentPath.node.children.splice(parentPath.index, 1);
  }

  let component_component = {};
  let component_comp_scripts = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'component_constructor_script']
  ], true);
  l = component_comp_scripts.length;
  for (let i = 0; i < l; i++) {
    let tag = ast_tag_objectify(component_comp_scripts[i].node);
    let names = tag.src.replace(/\/components\/components\//, '').split('%7C');
    let nl = names.length;
    let sl = nl;
    for (let x = 0; x < nl; x++) {
      if (names[x] in component_component) {
        names.splice(x, 1);
        x--;
        nl--;
      } else {
        component_component[names[x]] = true;
      }
    }
    if (nl !== sl) {
      if (nl === 0) {
        remove.push(component_comp_scripts[i]);
      } else {
        let newhref = '/components/components/' + names.join('%7C');
        let param = ast_finder(component_comp_scripts[i].node, [
          ['type', 'tag_parameter'],
          ['name', 'src']
        ], false);
        param[0].node.children[0].content = newhref;
      }
    }
    component_comp_scripts[i].node.name = 'script';
  }
  while (remove.length > 0) {
    let temp = remove.pop();
    let parentPath = temp.path[temp.path.length - 1];
    parentPath.node.children.splice(parentPath.index, 1);
  }

  let slots = ast_finder(ast, [
    ['type', 'tag'],
    ['name', 'sl-t--']
  ], true, true);
  l = slots.length;
  for (let i = 0; i < l; i++) {
    slots[i].node.name = 'sl-t';
  }

  slots = ast_finder(ast, [
    ['type', 'tag_parameter'],
    ['name', 'sl-t--']
  ], true, true);
  l = slots.length;
  for (let i = 0; i < l; i++) {
    slots[i].node.name = 'sl-t';
  }

  return ast;
}

async function compile_file_tree(filepath, callback) {
  let source = "";
  if (!options.viewPath) {
    options.viewPath = filepath.replace(/([\/\\])[^\/\\]+$/, "");
  }
  inherit_tree(filepath, {
    _list: []
  }).then((ast) => {
    //get rid of duplicates in component file trees
    duplicate_component_removal(ast).then((ast) => {
      let name = path.basename(filepath).replace(/\.[^]*/, '');
      compile_file(name, ast, function (err, func) {
        if (err) {
          callback(err);
        } else {
          callback(null, func);
        }
      });
    });
  }).catch((err) => {
    callback(err);
  });
}

function handleCache(filepath, Options, callback) {
  if (viewCache[filepath] === undefined || options.debug === true) {
    if (typeof Options.compileDebug !== 'undefined' && options.set != true) {
      options.debug = compileDebug;
    }
    compile_file_tree(filepath, (err, fn) => {
      if (err) {
        options.log(err);
        callback(err);
      } else {
        if (!options.debug) {
          viewCache[filepath] = fn;
        }
        fn(Options, callback);
      }
    });
  } else {
    viewCache[filepath](Options, callback);
  }
}

function renderFile(filepath, options, callback) {
  if (callback === undefined && typeof options === 'function') {
    callback = options;
    options = {};
  }
  if (options.compileDebug === undefined && process.env.NODE_ENV === 'production') {
    options.debug = false;
  }
  handleCache(filepath, options, callback);
}

function baseJs() {
  return `
  function ${options.clientVar}_ready_element(comp,mounted){
    if(!('${options.clientVar}' in comp && mounted !== true)){
      var l = comp.classList.length;
      var onCreated_functions = [];
      var onMounted_functions = [];
      var supply_functions = [];
      var version = comp.getAttribute('version') || comp.getAttribute('v') || 'latest';
      var mainComp = undefined;
      for(var i = 0; i < l; i++){
        var comp_class = comp.classList[i];
        var featureVersion = (comp_class.match(/_([^]+)$/)||['','latest'])[1];
        comp_class = comp_class.replace(/_[^]+$/, '');
        if(comp_class in ${options.clientVar}._components && version in ${options.clientVar}._components[comp_class]){
          var tempBasePointer = ${options.clientVar}._components[comp_class][version];
          var tobj = comp['${options.clientVar}'];
          if(!('${options.clientVar}' in comp)){
            tobj = Object.create(tempBasePointer);
            tobj[0] = comp;
            tobj.classes = comp.classList;
            tobj.styles = comp.style;
            if(typeof tobj._data === 'undefined'){
              tobj._data = {};
            }
            else{
              tobj._data = Object.assign({},tobj._data);
            }
            tobj._attr = {};
            comp.${options.clientVar} = tobj;
            tobj.valueOf = comp;
            if('onCreated' in tempBasePointer){
              onCreated_functions.unshift([tobj,tempBasePointer.onCreated]);
            }
          }
          if(mounted){
            if('onMounted' in tempBasePointer || 'ready' in tempBasePointer){
              onMounted_functions.unshift([tobj,tempBasePointer.onMounted||tempBasePointer.ready]);
            }
            if('supply' in tempBasePointer){
              supply_functions.unshift([tobj,tempBasePointer.supply]);
            }
          }
          mainComp = tobj;
        }
        else if(comp_class in ${options.clientVar}._features && featureVersion in ${options.clientVar}._features[comp_class]){
          if(!('${options.clientVar}_features' in comp)){
            comp.${options.clientVar}_features = {};
          }
          var tempBasePointer = ${options.clientVar}._features[comp_class][featureVersion];
          var tobj = comp.${options.clientVar}_features[comp_class];
          if(!(comp_class in comp.${options.clientVar}_features)){
            var tobj = Object.create(tempBasePointer);
            tobj[0] = comp;
            tobj.classes = comp.classList;
            tobj.styles = comp.style;
            if(typeof tobj._data === 'undefined'){
              tobj._data = {};
            }
            else{
              tobj._data = Object.create(null,tobj._data);
            }
            tobj._attr = {};
            comp.${options.clientVar}_features[comp_class] = tobj;
            tobj.valueOf = comp;
            if('onCreated' in tempBasePointer){
              onCreated_functions.push([tobj,tempBasePointer.onCreated]);
            }
          }
          if(mounted){
            if('onMounted' in tempBasePointer || 'ready' in tempBasePointer){
              onMounted_functions.push([tobj,tempBasePointer.onMounted||tempBasePointer.ready]);
            }
            if('supply' in tempBasePointer){
              supply_functions.push([tobj,tempBasePointer.supply]);
            }
          }
        }
      }
      while(onCreated_functions.length){
        var created = onCreated_functions.shift();
        created[1].call(created[0],mainComp);
      }
      if(mounted){
        while(supply_functions.length){
          var supply = supply_functions.shift();
          supply[1].call(supply[0],mainComp);
        }
        while(onMounted_functions.length){
          var mount = onMounted_functions.shift();
          mount[1].call(mount[0],mainComp);
        }
        if(mainComp){
          setTimeout(function(){
            mainComp.trigger('mounted');
          },60);
        }
      }
      return mainComp;
    }
    return null;
  }


  var ${options.clientVar}_unmount_element = (function(){
    var unmountSet = new Set();
    var callbackScheduled = false;
    var raf = (typeof window.requestAnimationFrame !== 'undefined')?window.requestAnimationFrame:function(cb){
      return setTimeout(cb,60);
    };

    function unmountProcessor(){
      var runSet = unmountSet;
      unmountSet = new Set();
      runSet.forEach(function(element){
        var comp = element.${options.clientVar};
        if('onUnmount' in comp){
          comp.onUnmount(comp);
        }
        if(element.${options.clientVar}_features){
          Object.keys(element.${options.clientVar}_features).forEach(function(featureName){
            var feature = element.${options.clientVar}_features[featureName];
            if('onUnmount' in feature){
              featture.onUnmount(comp);
            }
          });
        }
      });
      callbackScheduled = false;
    }

    return function(element){
      unmountSet.add(element);
      if(callbackScheduled === false){
        raf(unmountProcessor);
        callbackScheduled = true;
      }
    };
  })()
  
  var ${options.clientVar}_subtree_updated_element = (function(){
    var updateSet = new Set();
    var callbackScheduled = false;
    var raf = (typeof window.requestAnimationFrame !== 'undefined')?window.requestAnimationFrame:function(cb){
      return setTimeout(cb,60);
    };

    function updateProcessor(){
      var runSet = updateSet;
      updateSet = new Set();
      runSet.forEach(function(element){
        $$(element).first().trigger('subtree-modified',{element:element});
      });
      callbackScheduled = false;
    }

    return function(element){
      updateSet.add(element);
      if(callbackScheduled === false){
        raf(updateProcessor);
        callbackScheduled = true;
      }
    };
  })()

  var ${options.clientVar}_mutation_observer = new MutationObserver(function(mutation_list){
    var l = mutation_list.length;
    for(var i = 0; i < l; i++){
      var addedNodes = mutation_list[i].addedNodes;
      var removedNodes = mutation_list[i].removedNodes;
      var max = addedNodes.length;
      for(var x = 0; x < max; x++){
        if(addedNodes[x].classList && addedNodes[x].classList.contains('aleet-comp')){
          ${options.clientVar}_ready_element(addedNodes[x],true);
        }
        if(addedNodes[x].getElementsByClassName){
          var descendantComponents = addedNodes[x].getElementsByClassName('aleet-comp');
          Array.prototype.slice.call(descendantComponents).forEach(function(elem){
            ${options.clientVar}_ready_element(elem,true);
          });
        }
      }
    }
  });

  ${options.clientVar}_mutation_observer.observe(document,{
    childList: true,
    subtree: true
  });
  
  var ${options.clientVar} = {
    '_components':{},
    '_features':{},
  };

  ${options.clientVar}._wait_for_list = {
    'component':{},
    'feature':{}
  };

  ${options.clientVar}._loading = {

  }

  ${options.clientVar}.inverted_promise_obj = function(){
    var out = {};
    out.promise = new Promise(function(res,rej){
      out.resolve = res;
      out.reject = rej;
    });
    return out;
  }

  ${options.clientVar}.register_component = function(name,version,object){
    if(typeof version === 'object' && typeof object ==='undefined'){
      object = version;
      version = 'latest';
    }
    if(!(name in ${options.clientVar}._components)){
      ${options.clientVar}._components[name] = {};
    }
    var tempObj = Object.create(${options.clientVar}._base_obj);
    var setLatest = false;
    ${options.clientVar}._components[name][version] = Object.assign(tempObj,object);
    if (${options.clientVar}._components[name]._latest !== 'latest' && !(${options.clientVar}._components[name]._latest > version)) {
      ${options.clientVar}._components[name]._latest = version;
      if (version !== 'latest') {
        setLatest = true;
        ${options.clientVar}._components[name]['latest'] = ${options.clientVar}._components[name][version];
      }
    }
    if(
      ${options.clientVar}._wait_for_list['component'][name] && 
      ${options.clientVar}._wait_for_list['component'][name][version]
    )
    ${options.clientVar}._wait_for_list['component'][name][version].resolve();
    if(
      setLatest &&
      ${options.clientVar}._wait_for_list['component'][name] && 
      ${options.clientVar}._wait_for_list['component'][name]['latest']
    )
    ${options.clientVar}._wait_for_list['component'][name]['latest'].resolve();
  }

  ${options.clientVar}.register_feature = function(name,version,object){
    if(typeof version === 'object' && typeof object ==='undefined'){
      object = version;
      version = 'latest';
    }
    if(!(name in ${options.clientVar}._features)){
      ${options.clientVar}._features[name] = {};
    }
    var tempObj = Object.create(${options.clientVar}._base_obj);
    var setLatest = false;
    ${options.clientVar}._features[name][version] = Object.assign(tempObj,object);
    if (${options.clientVar}._features[name]._latest !== 'latest' && !(${options.clientVar}._features[name]._latest > version)) {
      ${options.clientVar}._features[name]._latest = version;
      if (version !== 'latest') {
        setLatest = true;
        ${options.clientVar}._features[name]['latest'] = ${options.clientVar}._features[name][version];
      }
    }
    if(
      ${options.clientVar}._wait_for_list['component'][name] && 
      ${options.clientVar}._wait_for_list['component'][name][version]
    )
    ${options.clientVar}._wait_for_list['component'][name][version].resolve();
    if(
      setLatest &&
      ${options.clientVar}._wait_for_list['component'][name] && 
      ${options.clientVar}._wait_for_list['component'][name]['latest']
    )
    ${options.clientVar}._wait_for_list['component'][name]['latest'].resolve();
  }

  ${options.clientVar}.register_constructor = function(name,version,con){
    if(typeof version === 'object' && typeof object ==='undefined'){
      object = version;
      version = 'latest';
    }
    var handle = setInterval(function(){
      try{
        var type = (typeof ${options.clientVar}._components[name] === 'undefined'?(typeof ${options.clientVar}._features[name] === 'undefined' ? "" : "feature"):"component");
        if (type) {
          ${options.clientVar}['_'+type+'s'][name][version].new = con;
          ${options.clientVar}._constructor_loaded(name,version,{type:type});
          if (version !== 'latest' && ${options.clientVar}['_'+type+'s'][name]._latest === version) {
            ${options.clientVar}['_'+type+'s'][name]['latest'].new = con;
            ${options.clientVar}._constructor_loaded(name,'latest',{type:type});
          }
          clearInterval(handle);
        }
      }
      catch(e){
      }
    },50);
  }

  ${options.clientVar}.register_feature = function(name,version,object){
    if(typeof version === 'object' && typeof object ==='undefined'){
      object = version;
      version = 'latest';
    }
    if(!(name in ${options.clientVar}._features)){
      ${options.clientVar}._features[name] = {};
    }
    var tempObj = Object.create(${options.clientVar}._base_obj);
    ${options.clientVar}._features[name][version] = Object.assign(tempObj,object);
    if(
      ${options.clientVar}._wait_for_list['component'][name] && 
      ${options.clientVar}._wait_for_list['component'][name][version]
    )
    ${options.clientVar}._wait_for_list['component'][name][version].resolve();
  }

  ${options.clientVar}._constructor_loaded = function(name,version,options){
    var type = 'component';
    if(
      ${options.clientVar}._wait_for_list[type][name] && 
      ${options.clientVar}._wait_for_list[type][name][version] && 
      ${options.clientVar}._wait_for_list[type][name][version].con
    ){
      ${options.clientVar}._wait_for_list[type][name][version].con.resolve();
    }
  }

  ${options.clientVar}.wait_for = function(name,version,options){
    if(typeof version === 'object' && typeof options ==='undefined'){
      options = version;
      version = 'latest';
    }
    var type = 'component';
    var constructor = options.constructor===true;
    if(typeof ${options.clientVar}._wait_for_list[type][name] === 'undefined'){
      ${options.clientVar}._wait_for_list[type][name] = {};
    }
    if(typeof ${options.clientVar}._wait_for_list[type][name][version] === 'undefined'){
      ${options.clientVar}._wait_for_list[type][name][version] = ${options.clientVar}.inverted_promise_obj();
      if(
        ${options.clientVar}['_'+type+'s'][name] && 
        ${options.clientVar}['_'+type+'s'][name][version]
      )
      ${options.clientVar}._wait_for_list[type][name][version].resolve();
    }
    if(constructor && typeof ${options.clientVar}._wait_for_list[type][name][version].con === 'undefined'){
      ${options.clientVar}._wait_for_list[type][name][version].con = ${options.clientVar}.inverted_promise_obj();
      if(
        ${options.clientVar}['_'+type+'s'][name] && 
        ${options.clientVar}['_'+type+'s'][name][version] && 
        ${options.clientVar}['_'+type+'s'][name][version].new
      )
      ${options.clientVar}._wait_for_list[type][name][version].con.resolve();
    }
    if(constructor) return ${options.clientVar}._wait_for_list[type][name][version].con.promise;
    else return ${options.clientVar}._wait_for_list[type][name][version].promise;
  }

  ${options.clientVar}._data = function(prop,value){
    if(typeof value !== 'undefined'){
      this._data[prop] = value;
      if(typeof this['on_'+prop+'_change'] !== 'undefined' && typeof value !== 'undefined'){
        this['on_'+prop+'_change'].call(this);
      }
    }
    if('${options.clientVar}_features' in this.elem()){
      var features = this.elem().${options.clientVar}_features;
      var l = features.length;
      for(var i = 0; i < l; i++){
        var feature = features[i];
        var type = 'string';
        if(typeof feature['on_'+prop+'_change'] !== 'undefined' && typeof value !== 'undefined'){
          feature['on_'+prop+'_change'].call(feature);
        }
      }
    }
    return this._data[prop];
  }

  ${options.clientVar}._attr = function(attr,value){
    var type = 'string'
    if(this.attributes && attr in this.attributes){
      type = this.attributes[attr];
    }
    if(typeof value !== 'undefined'){
      if(type === "Json"){
        value = JSON.stringify(value);
      }
      this._attr[attr] = value+"";
      this[0].setAttribute(attr,value);
      if(typeof this['on_'+attr+'_change'] !== 'undefined'){
        this['on_'+attr+'_change'].call(this);
      }
    }
    var val = this[0].getAttribute(attr);
    if(type === "Json"){
      val = JSON.parse(val);
    }
    else if(type === "Number"){
      val = parseFloat(val);
    }
    if('${options.clientVar}_features' in this.elem()){
      var features = this.elem().${options.clientVar}_features;
      var l = features.length;
      for(var i = 0; i < l; i++){
        var feature = features[i];
        if(typeof feature['on_'+attr+'_change'] !== 'undefined' && typeof value !== 'undefined'){
          feature['on_'+attr+'_change'].call(feature);
        }
      }
    }
    return val;
  }

  ${options.clientVar}._on = function(event,handler,options){
    options = options||{};
    var lock = false;
    var _self = this;
    var events = [];
    if(!isNaN(options.debounceTime) && options.debounceTime > 0){
      this[0].addEventListener(event,function(e){
        if(lock === false){
          lock = setTimeout(function(){
            lock = false;
            handler.call(_self,events);
            events = [];
          },options.debounceTime);
        }
        events.push(e);
      });
    }
    else if(!isNaN(options.idleFireTime) && options.idleFireTime > 0){
      this[0].addEventListener(event,function(e){
        if(lock){
          clearTimeout(lock);
        }
        lock = setTimeout(function(){
          lock = false;
          handler.call(_self,events);
          events = [];
        },options.idleFireTime);
        events.push(e);
      });
    }
    else{
      this[0].addEventListener(event,function(e){
        handler.call(_self,e);
      });
    }
    return this;
  }

  ${options.clientVar}._trigger = function(event,data){
    var e;
    try{
      e = new Event(event,{
        bubbles: true,
        cancelable: true
      });
    }
    catch(err){
      e = document.createEvent('Event');
      e.initEvent(event, true, true);
    }
    if(data){
      Object.keys(data).forEach(function(data_key){
        e[data_key] = data[data_key];
      });
    }
    this[0].dispatchEvent(e);
    return this;
  }

  ${options.clientVar}._slot = function(name){
    if('_slots' in this && name in this._slots){
      return this._slots[name];
    }
    else{
      if(!('_slots' in this)){
        this._slots = {};
      }
      function elem(input){
        return input.elem();
      }
      var innerSlots = this.${options.selectorVar}('sl-t, [sl-t]');
      if(this.attr('sl-t')) innerSlots.push(this);
      var underSlots = this.${options.selectorVar}('.aleet-comp sl-t, .aleet-comp [sl-t], aleet-comp[sl-t]');
      var slots = $$($$.difference(innerSlots.map(elem),underSlots.map(elem)));
      var l = slots.length;
      for(var i = 0; i < l; i++){
        var slot_name = slots[i].attr('name');
        if(slots[i].elem().tagName.toLowerCase() !== 'sl-t') slot_name = slots[i].attr('sl-t');
        this._slots[slot_name] = slots[i];
      }
      if(name in this._slots){
        return this._slots[name];
      }
      else{
        return undefined;
      }
    }
  }

  ${options.clientVar}._remove_children = function(){
    var node = this.elem();
    var last;
    while (last = node.lastChild) node.removeChild(last);
    return this;
  }

  ${options.clientVar}._append_child = function(child){
    if(child._isAleetComponent && child.elem) child = child.elem();
    this.elem().appendChild(child);
    return this;
  }

  ${options.clientVar}._prepend_child = function(child){
    if(child._isAleetComponent && child.elem) child = child.elem();
    var node = this.elem();
    if(node.firstChild){
      node.insertBefore(child,node.firstChild);
    }
    else{
      node.appendChild(child);
    }
    return this;
  }

  ${options.clientVar}.import = function(name){
    console.warn('Is Deprecated, Will Be Removed In Future Update');
    var css = document.createElement('link');
    css.href = '/components/css/'+(name).replace(/[\\]|[\/]/g,'%2F').replace(/[\\.]/g,'%2E');
    css.type = "text/css";
    css.rel = "stylesheet";
    document.getElementsByTagName("head")[0].appendChild(css);
    var js = document.createElement('script');
    js.src = '/components/js/'+(name).replace(/[\\]|[\/]/g,'%2F').replace(/[\\.]/g,'%2E');
    document.getElementsByTagName("body")[0].appendChild(js);
    var comp = document.createElement('script');
    comp.src = '/components/components/'+(name).replace(/[\\]|[\/]/g,'%2F').replace(/[\\.]/g,'%2E');
    document.getElementsByTagName("body")[0].appendChild(comp);
  }

  ${options.clientVar}.load = function(name, version){
    version = version || 'latest';
    var fullName = name+'---'+version;
    if (${options.clientVar}._loading[fullName]) return ${options.clientVar}._loading[fullName];
    ${options.clientVar}._loading[fullName] = new Promise(function(resolve, reject) {
      ${options.selectorVar}.ajax({
        url: '/components/components/'+(fullName).replace(/[\\.]/g,'%2E')+'?t=query_required',
        success: function(response) {
          var returnedData = JSON.parse(response);
          if (returnedData.success) {
            var requiredComponents = returnedData.requiredComponents||[];
            var requiredConstructors = returnedData.requiredConstructors||[];
            var requiredScripts = returnedData.requiredScripts||[];
            var requiredStylesheets = returnedData.requiredStylesheets||[];
            requiredComponents.push(fullName);
            requiredConstructors.push(fullName);
            var fragment = document.createDocumentFragment();
            requiredStylesheets.forEach(function(stylesheet) {
              if (document.querySelector('link[href*="'+stylesheet+'"]')) return;
              var link = document.createElement('link');
              link.setAttribute('rel','stylesheet');
              link.setAttribute('href',stylesheet);
              fragment.appendChild(link);
            });
            requiredScripts.forEach(function(script) {
              if (document.querySelector('script[src*="'+script+'"]')) return;
              var scriptElement = document.createElement('script');
              scriptElement.setAttribute('src', script);
              fragment.appendChild(script);
            });
            requiredComponents = requiredComponents.filter(function(component) {
              if (document.querySelector('script[src*="'+component+'"][src*="/components/js"]')) return false;
              return true;
            });
            requiredConstructors = requiredConstructors.filter(function(constructor) {
              if (document.querySelector('script[src*="'+constructor+'"][src*="/components/component"]')) return false;
              return true;
            });
            if (requiredComponents.length > 0) {
              var scriptElement = document.createElement('script');
              var linkElement = document.createElement('link');
              linkElement.setAttribute('rel', 'stylesheet');
              var names = requiredComponents.join('|').replace(/\\|/g,'%7C').replace(/[\\.]/g,'%2E');
              scriptElement.setAttribute('src','/components/js/'+names);
              linkElement.setAttribute('href', '/components/css/'+names);
              fragment.appendChild(linkElement);
              fragment.appendChild(scriptElement);
            }
            if (requiredConstructors.length > 0) {
              var scriptElement = document.createElement('script');
              var names = requiredConstructors.join('|').replace(/\\|/g,'%7C').replace(/[\\.]/g,'%2E');
              scriptElement.setAttribute('src','/components/components/'+names);
              fragment.appendChild(scriptElement);
            }
            document.body.appendChild(fragment);
            ${options.clientVar}.wait_for(name, version, {
              constructor: true
            }).then(function() {
              ${options.clientVar}._loading[fullName] = undefined;
              resolve();
            });
          } else {
            ${options.clientVar}._loading[fullName] = undefined;
            reject(new Error(returnedData.error));
          }
        },
        error: function(error) {
          ${options.clientVar}._loading[fullName] = undefined;
          reject(error);
        }
      });
    });
    return ${options.clientVar}._loading[fullName];
  }

  ${options.clientVar}.queryState = function(name, version) {
    version = version || 'latest';
    var fullName = name+'---'+version;
    if (${options.clientVar}._loading[fullName]) {
      return 'loading';
    } if (document.querySelector('script[src*="'+fullName+'"][src*="/components/js"]')) {
      return 'loaded';
    } else {
      return 'not found';
    }
  }

  ${options.clientVar}.new = function(name,version,options){
    if(typeof version === 'object' && typeof options ==='undefined'){
      options = version;
      version = 'latest';
    }
    if(${options.clientVar}._components[name] && typeof ${options.clientVar}._components[name][version].new === 'function'){
      return ${options.clientVar}_ready_element(${options.clientVar}._components[name][version].new(options));
    }
    else if(${options.clientVar}._features[name] && typeof ${options.clientVar}._features[name][version].new === 'function'){
      return ${options.clientVar}_ready_element(${options.clientVar}._features[name][version].new(options));
    }
  }

  ${options.clientVar}._base_obj = {
    ${options.selectorVar}: ${options.selectorVar},
    data: ${options.clientVar}._data,
    attr: ${options.clientVar}._attr,
    on: ${options.clientVar}._on,
    trigger: ${options.clientVar}._trigger,
    elem: function(){return this[0];},
    slot: ${options.clientVar}._slot,
    empty: ${options.clientVar}._remove_children,
    append: ${options.clientVar}._append_child,
    prepend: ${options.clientVar}._prepend_child,
    _isAleetComponent:true
  };
  
  function ${options.selectorVar}(selector){
    var base = document;
    if(typeof this.${options.selectorVar} !== 'undefined' && this !== window){
      base = this[0];
      base.classList.add('aleet_internal_selector_class');
      selector = selector.split(',').map(function(piece){ return '.aleet_internal_selector_class '+piece }).join(',');
    }
    var results = selector;
    if(typeof results === "string" || results instanceof String){
      results = base.querySelectorAll(selector);
      if(base !== document){
        base.classList.remove('aleet_internal_selector_class');
      }
    }
    if(results instanceof HTMLElement || (!('length' in results) && !Array.isArray(results))){
      results = [results];
    }
    var objs = [];
    Object.assign(objs,${options.selectorVar}._base_obj);
    for(var i = results.length-1; i >= 0; i--){
      if(results[i].${options.clientVar}){
        objs[i] = results[i].${options.clientVar};
      }
      else if(results[i].${options.selectorVar}){
        objs[i] = results[i];
      }
      else{
        objs[i] = Object.create(${options.clientVar}._base_obj);
        objs[i][0] = results[i];
        objs[i]._data = {};
        objs[i]._attr = {};
        objs[i].classes = results[i].classList;
        objs[i].styles = results[i].style;
      }
    }
    objs.first = function(){return this.length > 0 ? this[0] : undefined;};
    if(objs.length){
      objs.classes = objs[0].classes;
      objs.styles = objs[0].styles;
    }
    return objs;
  }

  ${options.selectorVar}._base_obj = {
    ${options.selectorVar}: function(a,b,c){
      return this[0].${options.selectorVar}(a,b,c);
    },
    data: function(a,b,c){
      return this[0].data(a,b,c);
    },
    attr: function(a,b,c){
      return this[0].attr(a,b,c);
    },
    on: function(a,b,c){
      return this[0].on(a,b,c);
    },
    trigger: function(a,b,c){
      return this[0].trigger(a,b,c);
    },
    elem: function(a,b,c){
      return this[0].elem(a,b,c);
    },
    slot: function(a,b,c){
      return this[0].slot(a,b,c);
    }
  };

  ${options.selectorVar}.ajax = function(options){
    var xhttp = new XMLHttpRequest();
    var method = options.method || "GET";
    var url = options.url;
    var formData = "";
    var content_type = options.content_type || "application/x-www-form-urlencoded";
    if(/post/i.test(method)){
      formData = new FormData();
      var keys = Object.keys(options.data);
      var l = keys.length;
      for(var i = 0; i < l; i++){
        formData.append(keys[i],options.data[keys[i]]);
      }
    }
    xhttp.onreadystatechange = function(){
      if(xhttp.readyState === XMLHttpRequest.DONE){
          if(xhttp.status === 200){
              if(typeof options.success === 'function'){
                options.success(xhttp.responseText);
              }
          }
          else{
            if(typeof options.error === 'function'){
              options.error(xhttp.responseText);
            }
          }
      }
    }
    xhttp.onerror = function(e){
      if(typeof options.error === 'function'){
        options.error(e);
      }
    }
    xhttp.open(method,url,true);
    xhttp.send(formData);
  }

  ${options.selectorVar}.simpleId = function(){
    return '_'+(Date.now()*1+''+Math.random().toFixed(10).substring(2));
  }

  ${options.selectorVar}.union = function(arr1,arr2){
    return arr1.concat(arr2.filter(function(input){
      return (arr1.indexOf(input) === -1) ? true : false;
    }));
  }

  ${options.selectorVar}.intersection = function(arr1,arr2){
    return arr1.filter(function(input){
      return (arr2.indexOf(input) !== -1) ? true : false;
    });
  }

  ${options.selectorVar}.difference = function(arr1,arr2){
    return arr1.filter(function(input){
      return (arr2.indexOf(input) === -1) ? true : false;
    });
  }

  document.addEventListener("DOMContentLoaded", function(){
    $$('.aleet-comp').forEach(function(elem){${options.clientVar}_ready_element(elem[0],true);});
  }, false);
  `;
}

function baseCss() {
  return `
    sl-t{
      display: block;
    }

    sl-t:empty{
      display: none;
    }

    sl-t:blank{
      display: none;
    }
  `;
}

async function js(req, res) {
  let out = '';
  if (req.params.name in jsCache) {
    out = jsCache[req.params.name];
  } else {
    if (req.params.name === "_base") {
      out = baseJs();
    } else {
      let names = req.params.name.split('|');
      let l = names.length;
      for (let i = 0; i < l; i++) {
        let name_version = names[i];
        let name = name_version.match(/^([^]+)---/)[1];
        let version = name_version.match(/^[^]+---([^]+)/)[1];
        let baseContents = await loadComponent(undefined, name, version);
        let componentContents = await inherit_tree('', {
          _list: []
        }, baseContents);
        let script = ast_finder(componentContents, [
          ['type', 'javascript_content']
        ], false);
        if (script.length) {
          out += script[0].node.content;
        }
      }
    }
    out = await options.jsProcessor(out);
    out = out.replace(/\uFFFF/g,'\\');
    if (!options.debug) {
      jsCache[req.params.name] = out;
    }
  }
  res.setHeader('Content-Type', 'text/javascript');
  res.end(out);
}

async function css(req, res) {
  let out = '';
  if (req.params.name in cssCache) {
    out = cssCache[req.params.name];
  } else {
    if (req.params.name === "_base") {
      out = baseCss();
    } else {
      let names = req.params.name.split('|');
      let l = names.length;
      for (let i = 0; i < l; i++) {
        let name_version = names[i];
        let name = name_version.match(/^([^]+)---/)[1];
        let version = name_version.match(/^[^]+---([^]+)/)[1];
        let baseContents = await loadComponent(undefined, name, version);
        let componentContents = await inherit_tree('', {
          _list: []
        }, baseContents);
        let css = ast_finder(componentContents, [
          ['type', 'css_content']
        ], false);
        if (css.length) {
          out += css[0].node.content.replace(/\[aleet-scoped]/g, '[aleet-scoped~="' + name_version + '"]');
        }
      }
    }
    out = await options.cssProcessor(out);
    out = out.replace(/\uFFFF/g,'\\');
    if (!options.debug) {
      cssCache[req.params.name] = out;
    }
  }
  res.setHeader('Content-Type', 'text/css');
  res.end(out);
}

async function components(req, res) {
  let out = '';
  if (req.params.name in componentCache && (!req.query.t)) {
    out = componentCache[req.params.name];
  } else {
    let names = req.params.name.split('|');
    let l = names.length;
    for (let i = 0; i < l; i++) {
      let name_version = names[i];
      let name = name_version.match(/^([^]+)---/)[1];
      let version = name_version.match(/^[^]+---([^]+)/)[1];
      let baseContents = await loadComponent(undefined, name, version);
      let componentContents = await inherit_tree('', {
        _list: []
      }, baseContents);
      if (req.query.t === 'query_required') {
        let returnObject = {
          success: true,
          requiredComponents: componentContents.requiredComps,
          requiredConstructors: componentContents.compConstructors,
          requiredScripts: componentContents.requiredScripts,
          requiredStylesheets: componentContents.requiredStylesheets
        };
        res.json(returnObject);
        return;
      }
      let template = ast_finder(componentContents, [
        ['type', 'tag'],
        ['name', 'template']
      ], false);
      let comp = ast_finder(template[0].node, [
        ['type', 'tag']
      ], false)[0];
      let tag_closing_bracket = ast_finder(comp.node, [
        ['type', 'tag_close']
      ], false, false)[0];
      let closePath = tag_closing_bracket.path[tag_closing_bracket.path.length - 1];
      //need to add data attributes to end of tag in copy
      closePath.node.children.splice(closePath.index, 0, preforms.component_data_print());
      let ast = {
        type: 'html',
        children: [comp.node]
      }
      let compiled = compile_file(name_version, ast, undefined, true).toString().replace(/sl-t--/g,'sl-t');
      let embed = `
      (function(){
        ${options.clientVar}.register_constructor("${name}", "${version}", ${compiled});
      })();
      `
      embed = embed.replace("function anonymous", "function");
      out += embed + '\n';
    }
    if (!options.debug) {
      componentCache[req.params.name] = out;
    }
  }
  res.setHeader('Content-Type', 'text/javascript');
  res.end(out);
}

async function config(Options) {
  let keys = Object.keys(options);
  for (let i = 0, l = keys.length; i < l; i++) {
    if (keys[i] in Options && keys[i] !== 'set') {
      options[keys[i]] = Options[keys[i]];
    }
  }
  options.set = true;
  let cachePath = options.cachePath || path.join(options.viewPath, 'cache');
  if (await fs.exists(cachePath)) {
    await fs.remove(cachePath)
  }
}

function removeFromCache(filepath){
  if(filepath){
    if(filepath in viewCache){
      viewCache[filepath] = undefined;
    }
  }
}

module.exports = renderFile;

module.exports.css = css;

module.exports.js = js;

module.exports.components = components;

module.exports.config = config;

module.exports.removeFromCache = removeFromCache;